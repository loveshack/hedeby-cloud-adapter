#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_startup_simhost.sh [-h] [-v]

Starts up a simhost.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_simhost_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_simhost_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

# general parameters
set_required SPOOL_DIR          "@@@spool_dir@@@"

set_optional SIMHOSTS_FILE_NAME "@@@simhosts_file_name@@@" "sdm_ec2.simhosts"
set_optional SIMHOST_SLEEP_TIME "@@@simhost_sleep_time@@@" 0
SIMHOSTS_FILE=$SPOOL_DIR/$SIMHOSTS_FILE_NAME

#################################################
log "Initializing simhosts"
touch $SIMHOSTS_FILE && chmod 0644 $SIMHOSTS_FILE || fail_no_undo "Could not create/access simhosts_file $SIMHOSTS_FILE"

# pretend to do some work
sleep $SIMHOST_SLEEP_TIME

# loop over available simhosts
#   if not already used as simhost => add to SIMHOSTS_FILE
FOUND=0
for res in `ypcat hosts | awk '{print $2}'` ; do
   if is_simhost $res ; then
      trace "simhost '$res' already used, trying next ..."
      continue
   fi

   trace "picked simhost '$res'"
   echo $res >> $SIMHOSTS_FILE
   if [ $? -ne 0 ]; then
      fail "Could not write into sim host file $SIMHOSTS_FILE"
   fi
   FOUND=1
   break
done

if [ $FOUND -ne 1 ] ; then
   fail_no_undo "No suitable simhost found."
fi

RES_resourceHostname="$res"
RES_simhost="true"

log "Started up simhost '$RES_resourceHostname'"

write_output_parameters_and_exit RES_resourceHostname RES_simhost
