#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# General utility functions for use in conjunction with the SDM cloud adapter.
#
# This is the version for using simhosts (simulated hosts) with the General
# Execution Framework (GEF).
#
# This shell script is sourced in from all other simhost-related shell scripts.


#---------------------------------------------------------------------------
#  NAME
#     is_simhost -- is a host a known simhost
#
#  SYNOPSIS
#     is_simhost host
#
#  INPUTS
#     host - name of the host to check
#
#  DESCRIPTION
#     This command returns true if 'host' is in the SIMHOSTS_FILE and thus
#     a known simhost.
is_simhost()
{
   if [ ! -n "$1" ]; then
      fatal "Internal error: is_simhost() called without required parameter"
   fi

   # grep in SIMHOSTS_FILE (one host per line)
   simhost_pattern="^$1\$"
   grep -s "$simhost_pattern" $SIMHOSTS_FILE > /dev/null 2>&1

   # return exit status of grep to indicate success/failure
   return $?
}

