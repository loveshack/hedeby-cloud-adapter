#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_trigger_ec2_host_startup.sh [-h] [-v]

Triggers the startup of a host on Amazon EC2.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_ec2_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ec2_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

# general parameters
set_required TMP_DIR     "@@@tmp_dir@@@"
set_required SYSTEM_NAME "@@@system_name@@@"
set_required CS_URL      "@@@cs_url@@@"

# ec2 parameters
set_required SECURITY_GROUP            "@@@security_group@@@"
set_required EC2_USER_KEY_PAIR         "@@@ec2_user_key_pair@@@"
set_required EC2_TOOLS_INSTALL_DIR     "@@@ec2_tools_install_dir@@@"
set_required EC2_USER_CERTIFICATE_FILE "@@@ec2_user_certificate_file@@@"
set_required EC2_USER_PRIVATE_KEY_FILE "@@@ec2_user_private_key_file@@@"
set_optional EC2_JAVA_HOME             "@@@ec2_java_home@@@" ""

# resource property parameters 
set_required RES_amiId             "@@@RESOURCE:amiId@@@"
set_optional DEFAULT_INSTANCE_TYPE "@@@instance_type@@@" "m1.small"
set_optional RES_instanceType      "@@@RESOURCE:instanceType@@@" "$DEFAULT_INSTANCE_TYPE"

log "Triggering startup of EC2 host (AMI-ID=$RES_amiId, instance type=$RES_instanceType) ..."

startup_output=`call_ec2 ec2-run-instances $RES_amiId \
   --instance-count 1 \
   --instance-type $RES_instanceType \
   --key $EC2_USER_KEY_PAIR \
   --group $SECURITY_GROUP`
rc=$?
if [ $rc -ne 0 ]; then
   # error in call to call_ec2 => propagate error
   exit $rc
fi

RES_instanceId=`get_instance_id "$startup_output"`
rc=$?
if [ $rc -ne 0 ] ; then
   # propagate error code
   exit $rc
fi
expr "$RES_instanceId" : "i-" > /dev/null
if [ "$?" -ne 0 ] ; then
   fail "Could not parse instance ID from EC2 tools command output. Got '$RES_instanceId' from '$startup_output'"
fi

log "Triggered startup of EC2 host with instance ID '$RES_instanceId'."

# we only set the instanceId resource property here, the rest of the
# properties are set in the next step
write_output_parameters_and_exit RES_instanceId
