#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_ec2_add_host_to_hosts_file.sh [-h] [-v]

Adds the host to the /etc/hosts file.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_ec2_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ec2_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

set_required RES_privateDnsName "@@@RESOURCE:privateDnsName@@@"
set_required RES_vpnIpAddress   "@@@RESOURCE:vpnIpAddress@@@"

set_optional HOSTS_FILE         "@@@hosts_file@@@"  "/etc/hosts"


log "Adding '$RES_privateDnsName' to hosts file '$HOSTS_FILE' ..."

#     Appends a line with "ip <tab> hostname" to the $HOSTS_FILE (/etc/hosts).
#
#     If the hostname contains a dot (~ is a FQDN), the line that is added to
#     the /etc/hosts file looks like this:
#     "ip <tab> short_hostname <tab> long_hostname"

if [ ! -w $HOSTS_FILE ] ; then
   fatal "Hosts file '$HOSTS_FILE' is not writeable."
fi

ip="$RES_vpnIpAddress"
hostname="$RES_privateDnsName"

if fgrep "$hostname" "$HOSTS_FILE" ; then
   # important: fail_no_undo, as the undo action would remove the entry from the hosts file!!
   fail_no_undo "Hosts file '$HOSTS_FILE' corrupted: host '$RES_privateDnsName' already exists."
fi

if echo $hostname | fgrep '.' > /dev/null 2>&1 ; then
   # hostname contains '.' => long hostname
   short_hostname=`echo $hostname | cut -f1 -d.`
   echo "$ip	$short_hostname	$hostname" >> $HOSTS_FILE
else
   echo "$ip	$hostname" >> $HOSTS_FILE
fi

log "Added '$RES_privateDnsName' to hosts file '$HOSTS_FILE'."
RES_resourceHostname="$hostname"

# set resourceHostname parameter to make resource bound
write_output_parameters_and_exit RES_resourceHostname
