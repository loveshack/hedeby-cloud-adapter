#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
ec2/gef_ec2_remove_host_from_hosts_file.sh [-h] [-v]

Remove the host from the /etc/hosts file.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_ec2_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ec2_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

# The hostname to remove.
set_required RES_resourceHostname "@@@RESOURCE:resourceHostname@@@"
hostname=$RES_resourceHostname

set_optional HOSTS_FILE "@@@hosts_file@@@"  "/etc/hosts"

log "Removing '$hostname' from file '$HOSTS_FILE' ..."

#     Deletes the respective line with "ip <tab> hostname" from the $HOSTS_FILE
#     (/etc/hosts) file. The line does not have to exist.
#
#     This removing is done with ed editor (and not with sed > tmp_file ; mv
#     tmp_file /etc/hosts) as on Solaris /etc/hosts is just a softlink to
#     /etc/inet/hosts which would be overridden by the mv command.
if [ ! -w $HOSTS_FILE ] ; then
   fatal "Hosts file '$HOSTS_FILE' is not writeable."
fi

# delete the corresponding line in the hosts file  by using the ed editor for
# inplace editing:
#     1) delete the first line that contains $hostname
#     2) write the file to disk
cat << EOINPUT | ed -s $HOSTS_FILE
/$hostname/d
w
EOINPUT

log "Removed '$hostname' from file '$HOSTS_FILE'."

# delete resourceHostname property
RES_resourceHostname=

write_output_parameters_and_exit hostname RES_resourceHostname
