#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_ec2_install_sdm_undo.sh [-h] [-v]

Shutdown the SDM system on the EC2 host by calling AMI script shutdown-host.sh
   o Shutdown JVM if necessary
   o Uninstall managed host

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_ec2_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ec2_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

set_required TMP_DIR          "@@@tmp_dir@@@"
set_required SYSTEM_NAME      "@@@system_name@@@"

# ec2 parameters
set_required EC2_USER_KEY_PAIR_FILE "@@@ec2_user_key_pair_file@@@"
set_ssh_commands

# resource properties might not be set (as this is undo)
set_optional RES_instanceId     "@@@RESOURCE:instanceId@@@"     ""
set_optional RES_dnsName        "@@@RESOURCE:dnsName@@@"        ""
set_optional RES_privateDnsName "@@@RESOURCE:privateDnsName@@@" ""

set_optional AMI_SCRIPTS_DIR    "@@@ami_scripts_dir@@@"   "/opt/sdm/util/cloud/ec2/ami_scripts"

if [ -z "$RES_dnsName" ] ; then
   if [ -z "$RES_privateDnsName" ] ; then
      log "Resource properties dnsName and privateDnsName not set, skipping uninstall of SDM system."
   else
      debug "Resource property dnsName not set, using privateDnsName '$RES_privateDnsName' instead"
      hostname="$RES_privateDnsName"
   fi
else
   hostname="$RES_dnsName"
fi


if [ -n "$hostname" ] ; then
   log "Executing SDM uninstall script on cloud host '$hostname' (instance id=$RES_instanceId) ..."

   # execute in undo_mode, use back ticks to avoid that ssh failure leads to step error
   output=`execute_ssh_script $hostname "shutdown-host.sh $SYSTEM_NAME undo_mode" 2>&1`
   rc=$?
   if [ "$rc" -ne 0 ]; then
      warn "Could not shutdown the SDM system, ssh reported error (rc=$rc)\n$output"
   fi 
fi

write_output_parameters_and_exit
