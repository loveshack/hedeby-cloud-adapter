#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_wait_for_instance_startup.sh [-h] [-v]

Waits until an ec2 instance is started up.

This script needs to be referenced from a waiting step.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_ec2_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ec2_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

# default parameters
set_required TMP_DIR          "@@@tmp_dir@@@"

# ec2 parameters
set_required EC2_TOOLS_INSTALL_DIR     "@@@ec2_tools_install_dir@@@"
set_required EC2_USER_CERTIFICATE_FILE "@@@ec2_user_certificate_file@@@"
set_required EC2_USER_PRIVATE_KEY_FILE "@@@ec2_user_private_key_file@@@"
set_required EC2_USER_KEY_PAIR_FILE    "@@@ec2_user_key_pair_file@@@"

set_optional EC2_JAVA_HOME             "@@@ec2_java_home@@@" ""

# resource properties
set_required RES_instanceId   "@@@RESOURCE:instanceId@@@"
set_required RES_amiId        "@@@RESOURCE:amiId@@@"
set_optional instance_type    "@@@instance_type@@@"         "m1.small"
set_optional RES_instanceType "@@@RESOURCE:instanceType@@@" "$instance_type"

log "Checking cloud host with instance ID '$RES_instanceId' for status 'running'..."

describe_output=`call_ec2 ec2-describe-instances $RES_instanceId`
rc=$?
if [ $rc -ne 0 ]; then
   # error in call to ec2 command => propagate error
   exit $rc
fi

if is_instance_running "$describe_output" ; then
   set_res_prop_from_output "$describe_output"

   vpn_short_private_dns_name=$RES_privateDnsName
   if echo $vpn_short_private_dns_name | fgrep '.' > /dev/null 2>&1 ; then
      # hostname contains '.' => long hostname
      vpn_short_private_dns_name=`echo $vpn_short_private_dns_name | cut -f1 -d.`
   fi

   log "EC2 cloud host '$RES_privateDnsName' (instance ID: $RES_instanceId) is in state running."

   # set resource properties that are needed by GE adapter
   RES_cloudResource=true
   RES_userKeyPairFile="$EC2_USER_KEY_PAIR_FILE"

   write_output_parameters_and_exit \
      RES_cloudResource \
      RES_dnsName RES_privateDnsName RES_keyName \
      RES_launchTime RES_availabilityZone        \
      RES_kernelId RES_ramdiskId                 \
      RES_monitoringState                        \
      RES_ipAddress RES_privateIpAddress         \
      vpn_short_private_dns_name                 \
      RES_userKeyPairFile
else
   rerun
fi
