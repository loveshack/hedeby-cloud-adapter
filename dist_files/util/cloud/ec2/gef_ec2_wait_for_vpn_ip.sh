#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_ec2_wait_for_vpn_ip.sh [-h] [-v]

The VPN IP address of the cloud host is extracted from the openVPN status log
file and stored in the resource property vpnIpAddress.

As the openVPN status log is only updated once every minute, this is
implemented as a waiting step.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_ec2_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ec2_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

set_required CS_URL      "@@@cs_url@@@"
set_required TMP_DIR     "@@@tmp_dir@@@"

# resource properties
set_required RES_instanceId     "@@@RESOURCE:instanceId@@@"
set_required RES_privateDnsName "@@@RESOURCE:privateDnsName@@@"
set_required client_name        "@@@vpn_short_private_dns_name@@@"

# The ifconfig pool persist and status-log information is stored in the OpenVPN
# config directory. These files should only exists once per VPN server, in the
# same way as the other VPN config files. Therefore they are stored in the config
# directory.
set_optional VPN_CONFIG_DIR "@@@vpn_config_dir@@@" "/etc/openvpn/easy-rsa/2.0"
VPN_STATUS_LOG="$VPN_CONFIG_DIR/openvpn-status.log"

##########################################################################

log "Determining VPN IP address of cloud host '$client_name' (instance id=$RES_instanceId) ..."

if [ ! -r $VPN_STATUS_LOG ] ; then
   # status log file not there, so exit with error
   fail "VPN status log file '$VPN_STATUS_LOG' not readable."
fi


# get the VPN IP address from the VPN status log
#   awk searches between ROUTING and GLOBAL for a line that has the
#   RES_privateDnsName in its 2nd field (comma separated). It then outputs only
#   the VPN IP address (field 1).
RES_vpnIpAddress=`awk -F, "/^ROUTING/,/^GLOBAL/ {if (\\\$2 == \"$client_name\") {print \\\$1}}" $VPN_STATUS_LOG`
debug "Got VPN IP: '$RES_vpnIpAddress'"
if [ -z "$RES_vpnIpAddress" ] ; then
   log "VPN IP address not found in file '$VPN_STATUS_LOG'."
   rerun
else
   # VPN IP address determined, make sure it looks like a VPN IP
   expr "$RES_vpnIpAddress" : '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*' > /dev/null 2>&1
   rc=$?
   if [ $rc -ne 0 ] ; then
      fail "Error: Got private VPN IP address '$RES_vpnIpAddress' of host '$RES_privateDnsName'. This does not look like a VPN IP address"
   fi
   log "Found VPN IP address $RES_vpnIpAddress of cloud host '$RES_privateDnsName' (instance id=$RES_instanceId)."
   break
fi

# everything ok
write_output_parameters_and_exit RES_vpnIpAddress
