#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_ec2_startup_vpn_connection.sh [-h] [-v]

Creates a ssh tunnel to the cloud host, forwarding connections to port 1194
(openvpn) on the cloud host to port 1194 on this host (the VPN master host).
For cleaning up purposes the PID of the ssh tunnel is stored in the resource
property sshTunnelPid.

Afterwards the VPN client on the cloud host is started up. This assumes that
the required openvpn files (config and credentials) are already available on
the cloud host.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_ec2_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ec2_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

set_required CS_URL      "@@@cs_url@@@"
set_required TMP_DIR     "@@@tmp_dir@@@"

# ec2 parameters
set_required EC2_USER_KEY_PAIR_FILE "@@@ec2_user_key_pair_file@@@"
set_ssh_commands

# resource properties
set_required RES_instanceId     "@@@RESOURCE:instanceId@@@"
set_required RES_dnsName        "@@@RESOURCE:dnsName@@@"
set_required RES_privateDnsName "@@@RESOURCE:privateDnsName@@@"
set_required client_name        "@@@vpn_short_private_dns_name@@@"

set_optional VPN_BASE_IP        "@@@vpn_base_ip@@@" "10.8.0.0"
# vpn_part contains by default '10.8'
vpn_part=`expr "$VPN_BASE_IP" : '\([0-9]*\.[0-9]*\)'`
SDM_MASTER_VPN_IP=$vpn_part.0.1

# for execution of ssh script on AMI:
set_optional AMI_SCRIPTS_DIR       "@@@ami_scripts_dir@@@"        "/opt/sdm/util/cloud/ec2/ami_scripts"
set_optional VPN_REMOTE_CONFIG_DIR "@@@vpn_remote_config_dir@@@"  "/etc/openvpn/easy-rsa/2.0"

##########################################################################
log "Setting up ssh tunnel to cloud host '$RES_privateDnsName' (instance id: $RES_instanceId)..."

command="$SSH_COMMAND -q -R 1194:localhost:1194 -N $RES_dnsName"
$command &
RES_sshTunnelPid=$!
log "Trying to start ssh tunnel with PID=$RES_sshTunnelPid ..."
# write the PID out at once, as it's needed for the cleanup
write_output_parameters RES_sshTunnelPid

# we cannot check directly for a exit code from $command (in case of an error
# straight away). Before we grep the process table we wait a couple of seconds
# until the (potentially) <defunct> process is cleaned up.
sleep 10 
if is_ssh_tunnel_running ; then
   log "ssh tunnel started."
else
   fail "Could not create ssh tunnel to cloud host using command '$command'"
fi


##########################################################################
log "Starting up openvpn on cloud host '$RES_privateDnsName' (instance id: $RES_instanceId)..."

sdm_master_host=`expr "$CS_URL" : '\(.*\):'`
if [ -z "$sdm_master_host" ] ; then
   fatal "Could not extract SDM master hostname from cs_url '$CS_URL'"
fi

# the VPN IP address of the SDM master and the VPN server must be one and the
# same, meaning that the VPN server has to run on the SDM master
remote_config_file="$VPN_REMOTE_CONFIG_DIR/client.conf.$client_name"
execute_ssh_script $RES_dnsName "startup-vpn.sh $sdm_master_host $SDM_MASTER_VPN_IP $remote_config_file $SDM_MASTER_VPN_IP"

# everything ok
# we already wrote the output parameters
exit 0
