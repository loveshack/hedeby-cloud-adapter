#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# Commands to execute when starting up an EC2 host using GEF.
# This script works on all EC2 hosts.
#
# The VPN client is started up, the /etc/hosts file is changed and the script
# waits until the VPN master can be contacted.

# usage:
#   startup-client.sh sdm_master sdm_master_vpn_ip vpn_config_file vpn_server_vpn_ip
SDM_MASTER_HOST=$1
SDM_MASTER_VPN_IP=$2
VPN_CONFIG_FILE="$3"
VPN_SERVER_VPN_IP=$4

# source common variables
. ./util.sh

##########################################################################
# setup VPN
#   -> this assumes that the keys, certificates and config files necessary for
#      VPN were generated on the VPN server and copied to this host into the
#      VPN config directory

restart_tun_driver

#   -> startup VPN with configuration from config dir as daemon, logging goes
#      to syslog
cwd=`pwd`
cd $VPN_BIN_DIR
./openvpn --config "$VPN_CONFIG_FILE" --daemon
exit_code=$?
cd $cwd

if [ $exit_code -ne 0 ] ; then
   echo "Error in startup-vpn.sh: could not start openvpn with client config: $VPN_CONFIG_FILE" >&2
   exit $exit_code
fi

#   -> wait for startup of VPN by pinging the VPN server on the virtual IP
#      address
wait_for_ping $VPN_SERVER_VPN_IP "VPN server"

# patch /etc/hosts file
append_to_hosts_file $SDM_MASTER_VPN_IP $SDM_MASTER_HOST
