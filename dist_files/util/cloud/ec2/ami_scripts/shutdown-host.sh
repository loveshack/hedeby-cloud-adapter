#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# Commands to execute before the shutdown of this EC2 host.
#
# This script works on all EC2 hosts (VPN server and client). 
#
# No special care is taken about terminating the VPN connection. All running
# processes are terminated anyway on shutdown of the machine.
#
# After running this script, the EC2 host is terminated.

# usage:
#   shutdown-host.sh sdm_system undo_mode
#
# If undo_mode is set (to any non-empty value), this script does not produce
# any errors. This is used for undo/reset steps.
SDM_SYSTEM="$1"
UNDO_MODE="$2"

# source common variables
. ./util.sh

sdm_run_dir=/var/spool/sdm/$SDM_SYSTEM/run

# if the SDM run directory doesn't exist, the system is already uninstalled
# => we are done
[ -d "$sdm_run_dir" ] || exit 0

# Try to shut down the jvms on the host
#   use -d switch to see stacktrace in log files
$SDMADM -d -s $SDM_SYSTEM -p system shutdown_jvm -h localhost -all
# Do not check exit code here!!
# It might be, that the jvm was already shut down beforehand by the user
# => this should not lead to an error here and thus an abort of the SDM
# deinstallation. It is OK, as we still check the run directory afterwards.

# wait until run dir is empty to ensure JVM shutdown
tries=0
while true
do
   nr_entries=`ls $sdm_run_dir | wc -l`
   if [ $nr_entries -eq 0 ]; then
      break
   fi
   tries=`expr $tries + 1`
   if [ $tries -gt 4 ]; then
      if [ -n "$UNDO_MODE" ] ; then
         # do hard kill of JVMs (pid is on first line in pid files in run directory) ...
         kill -9 `head -n 1 -q $sdm_run_dir/*`
         # ... and remove files
         rm -f $sdm_run_dir/* 
         break
      else
         # report errors
         echo "Problem with shutting down JVM. Run dir '$sdm_run_dir' contains $nr_entries entries but should be empty."
         exit 88
      fi
   fi
   sleep 10
done

# The only thing left to do is to uninstall the managed host itself.
#   use -d switch to see stacktrace in log files
$SDMADM -d -s $SDM_SYSTEM -p system uninstall_host
rc=$?

if [ -n "$UNDO_MODE" ] ; then
   # don't report errors
   exit 0
else
   exit $rc
fi
