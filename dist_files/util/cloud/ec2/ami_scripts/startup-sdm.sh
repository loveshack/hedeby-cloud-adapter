#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# Commands to execute when installing the SDM system on the EC2 host.
#
#   -> this assumes that the SDM keystore and certificate are available in the
#      /root directory. The calling script has to make sure that this is the
#      case.
# 

# usage:
#   startup-sdm.sh sdm_system sdm_cs_url sdm_opt_args
SDM_SYSTEM=$1
SDM_CS_URL=$2
shift
shift
# for handling simple install and nossl system:
# additional options to managed host install command in command line format (-option)
SDM_OPT_ARGS="$@"

# source common variables
. ./util.sh


##########################################################################
# install managed host
sdm_keystore=/root/root.keystore
sdm_cacert=/root/cacert.pem

#   -> install managed host
#      use -d switch to see stacktrace in log files
$SDMADM -d -s $SDM_SYSTEM -p system -keystore $sdm_keystore -cacert $sdm_cacert install_managed_host -au root -cs_url "$SDM_CS_URL" $SDM_OPT_ARGS
exit_code=$?

#   -> remove certificate and keystore files, they are no longer needed
rm -f $sdm_keystore $sdm_cacert > /dev/null

if [ $exit_code -ne 0 ] ; then
   exit $exit_code
fi

#   -> make the installed system the default (not really necessary but handy
#      for manual testing on the instance)
$SDMADM -d -s $SDM_SYSTEM -p system set_default_bootstrap_config

#   -> startup the executor_vm
$SDMADM -d -s $SDM_SYSTEM -p system startup_jvm
exit_code=$?

if [ $exit_code -ne 0 ] ; then
   exit $exit_code
fi
