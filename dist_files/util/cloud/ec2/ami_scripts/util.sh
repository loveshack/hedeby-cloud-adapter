#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# commonly used variables
SDM_DIST_DIR=/opt/sdm
SDMADM=$SDM_DIST_DIR/bin/sdmadm
VPN_BIN_DIR=/opt/csw/sbin

#---------------------------------------------------------------------------
#  NAME
#     restart_tun_driver -- restarts the tun driver
#
#  SYNOPSIS
#     restart_tun_driver
#
#  DESCRIPTION
#   -> workaround for broken tun driver
#      (somehow the tun driver does not load after rebundling the image, this
#       workaround loads the driver and makes it show up with modinfo)
#      The rem_drv and add_drv commands produce errors while doing this, but
#      the end result is OK. The errors are suppressed.
#
#   WARNING: this routine works only on Solaris/OpenSolaris
restart_tun_driver()
{
   rem_drv tun > /dev/null 2>&1
   add_drv tun > /dev/null 2>&1
}


#---------------------------------------------------------------------------
#  NAME
#     append_to_hosts_file -- adds entry to /etc/hosts file
#
#  SYNOPSIS
#     append_to_hosts_file <ip> <hostname>
#
#  DESCRIPTION
#     adds an entry with IP and hostname to the /etc/hosts file
#
#     This is used for adding the SDM master host.
append_to_hosts_file()
{
   ip="$1"
   hostname="$2"
   hosts_file=/etc/hosts

   if [ -w $hosts_file ] ; then
      echo "# SDM master host" >> $hosts_file
      echo "$ip	$hostname"  >> $hosts_file
   else
      echo "Hosts file '$hosts_file' is not writeable, aborting."
      exit 1
   fi
}


#---------------------------------------------------------------------------
#  NAME
#     wait_for_ping -- wait until host is ping-able
#
#  SYNOPSIS
#     wait_for_ping host [host_description]
#
#  INPUTS
#     host - hostname or IP address to ping
#     host_description - name for the host to use in debug/error messages,
#                        defaults to value of host parameter if not given
#
#  DESCRIPTION
#     This function tries to ping a host repeatedly until the ping is
#     successful or more than max_tries tries have been executed.
#
#     As the ping commands are different on different OSes
#     (blocking/non-blocking behavior, command line switches) a distinction is
#     made here based on the output of the SDM arch script.
#
#     In case the effort is not successful, the script is stopped with error
#     code 40.
wait_for_ping()
{
   host="$1"
   host_description="${2:-$1}"
   max_tries=5

   # the arch script is in the util dir
   arch=`$SDM_DIST_DIR/util/arch`

   case $arch in
       sol*)    command="ping"       ;;
       lx*)     command="ping -w 10" ;;
       darwin*) command="ping -t 10" ;;
       *)       command="ping"       ;;
   esac

   cnt=1
   while true
   do
      if $command $host > /dev/null 2>&1 ; then
         break
      else
         sleep 5
         cnt=`expr $cnt + 1`
      fi

      if [ $cnt -gt $max_tries ] ; then
         echo "Error: ping unsuccessful - $host_description not reachable." >&2
         exit 40
      fi
   done
}

