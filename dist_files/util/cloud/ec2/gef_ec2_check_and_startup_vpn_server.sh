#!/bin/bash

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_check_and_startup_vpn_server.sh [-h] [-v]

Checks if the vpn server is running.

If not, the vpn server is started up. During the vpn server startup,
credentials are created if they do not exist already.

If the script exits with RC=0 it is ensured that the vpn server is running.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_ec2_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ec2_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

# for version check
set_required EC2_TOOLS_INSTALL_DIR     "@@@ec2_tools_install_dir@@@"

# The ifconfig pool persist and status-log information is stored in the OpenVPN
# config directory. These files should only exists once per VPN server, in the
# same way as the other VPN config files. Therefore they are stored in the config
# directory.
set_optional VPN_CONFIG_DIR "@@@vpn_config_dir@@@" "/etc/openvpn/easy-rsa/2.0"
OPENVPN_STATUS_LOG="$VPN_CONFIG_DIR/openvpn-status.log"
OPENVPN_IFCONFIG_POOL_PERSIST="$VPN_CONFIG_DIR/ifconfig-pool-persist.txt"

set_optional VPN_BIN_DIR      "@@@vpn_bin_dir@@@"     "/opt/csw/sbin"
set_optional VPN_CONFIG_DIR   "@@@vpn_config_dir@@@"  "/etc/openvpn/easy-rsa/2.0"
# write the pid of openvpn daemon to this file
set_optional VPN_PID_FILE     "@@@vpn_pid_file@@@"    "/var/run/openvpn.pid"
# the base IP of the class B subnet that is used for the VPN
set_optional VPN_BASE_IP      "@@@vpn_base_ip@@@"     "10.8.0.0"


if is_openvpn_running ; then
   # don't do anything
   log "VPN server is already running."
   exit 0
else
   # check that the right versions of the tools are installed (requires
   # EC2_TOOLS_INSTALL_DIR and VPN_BIN_DIR set).
   #
   # For performance reasons do this only when the VPN server needs to be started.
   check_openvpn_and_ec2_versions

   ##########################################################################
   log "Checking (and creating) VPN server credentials and config files ..."
   # check that all necessary credential and config files exist
   if [ \
        -s $VPN_CONFIG_DIR/server.conf          -a \
        -r $VPN_CONFIG_DIR/keys/ca.crt          -a \
        -r $VPN_CONFIG_DIR/keys/ca.key          -a \
        -r $VPN_CONFIG_DIR/keys/dh1024.pem      -a \
        -r $VPN_CONFIG_DIR/keys/serial             \
      ] ; then

      log "Using existing configuration ..."
   else
      log "Generating new configuration ..."

      if [ ! -d "$VPN_CONFIG_DIR" ] ; then
         fatal "OpenVPN config directory '$VPN_CONFIG_DIR' does not exist."
      fi

      # setup VPN
      KEY_COMMON_NAME=`hostname`

      # check that server configuration template exists
      # @@@include:./server.conf.template@@@
      server_config_template=./server.conf.template
      if [ ! -r "$server_config_template" ] ; then
         fatal "The template for the VPN server configuration does not exists (is not readable): <sdm_dist_dir>/util/cloud/ec2/server.conf.template"
      fi

      # -> generate server.conf from template, use s### for replacements that are path names and thus contain '/'
      # replace the following values:
      #    %%%server%%%                        - the name of the vpn server (for the .crt and .key filename)
      #    %%%vpn_config_dir%%%                - where the OpenVPN config is stored (must contain the build-ca, etc. scripts)
      #    %%%vpn_base_ip%%%                   - the base address of the VPN subnet (by default, this is 10.8.0.0)
      #    %%%openvpn_status_log%%%            - where to regularly store mapping information: hostname -> VPN IP
      #    %%%openvpn_ifconfig_pool_persist%%% - where to persist the hostname -> VPN IP mapping (over VPN server restarts)
      sed -e "s/%%%server%%%/$KEY_COMMON_NAME/g" \
          -e "s#%%%vpn_config_dir%%%#$VPN_CONFIG_DIR#g" \
          -e "s/%%%vpn_base_ip%%%/$VPN_BASE_IP/" \
          -e "s#%%%openvpn_status_log%%%#$OPENVPN_STATUS_LOG#" \
          -e "s#%%%openvpn_ifconfig_pool_persist%%%#$OPENVPN_IFCONFIG_POOL_PERSIST#" \
            $server_config_template > $VPN_CONFIG_DIR/server.conf
      exit_code=$?
      if [ $exit_code -ne 0 ] ; then
         fail "could not create server config (server.conf), exit code=$exit_code"
      fi

      cd $VPN_CONFIG_DIR
      . ./vars >&2

      # Check that OPENSSL variable sourced from ./vars points to an executable openssl.
      # The easy-rsa 2.0 scripts sadly don't check that themselves, so we do it here
      # for them.
      output=`$OPENSSL version 2>&1`
      rc=$?
      if [ $rc -ne 0 ] ; then
         fail "openssl binary '$OPENSSL' not found in path. Please set the correct path to openssl in $VPN_CONFIG_DIR/vars (variable OPENSSL)."
      fi

      output=`./clean-all 2>&1`
      exit_code=$?
      if [ $exit_code -ne 0 ] ; then
         fail_no_undo "could not create VPN setup: clean-all failed with exit code=$exit_code\noutput=$output"
      fi

      ./build-ca
      exit_code=$?
      if [ $exit_code -ne 0 ] ; then
         fail "could not create VPN setup: build-ca failed with exit code=$exit_code"
      fi

      ./build-key-server $KEY_COMMON_NAME
      exit_code=$?
      if [ $exit_code -ne 0 ] ; then
         fail "could not create VPN setup: build-key-server failed with exit code=$exit_code"
      fi

      ./build-dh
      exit_code=$?
      if [ $exit_code -ne 0 ] ; then
         fail "could not create VPN setup: build-dh failed with exit code=$exit_code"
      fi

   fi

   #############################################
   log "Starting up VPN server ..."
   #   -> startup VPN server with configuration from config dir as daemon, logging
   #      goes to syslog
   debug "$VPN_BIN_DIR/openvpn --daemon --config $VPN_CONFIG_DIR/server.conf --writepid $VPN_PID_FILE"
   output=`$VPN_BIN_DIR/openvpn --daemon --config $VPN_CONFIG_DIR/server.conf --writepid $VPN_PID_FILE 2>&1`
   exit_code=$?
   if [ $exit_code -ne 0 ] ; then
      # some error conditions can be communicated via the exit code, even if openvpn daemonizes
      fail "Immediate failure while starting up openvpn server, exit code=$exit_code. Maybe a permission problem, take a look into the syslog.\noutput=$output"
   fi

   # check after 5 seconds that the openvpn service is really running
   sleep 5
   if ! is_openvpn_running ; then
        fatal "openvpn did not start, please check system logs.\nMay be there is no tun device available."
   fi
fi

# everything ok
write_output_parameters_and_exit
