#!/bin/bash

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_ec2_create_vpn_credentials.sh [-h] [-v]

Creates the credentials for a VPN client on the VPN master. The credentials are
copied to the client via scp.

The VPN connection is NOT initiated.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_ec2_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ec2_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

set_required TMP_DIR          "@@@tmp_dir@@@"

# ec2 parameters
set_required EC2_USER_KEY_PAIR_FILE "@@@ec2_user_key_pair_file@@@"
set_ssh_commands

# resource properties
set_required RES_instanceId     "@@@RESOURCE:instanceId@@@"
set_required client_name        "@@@vpn_short_private_dns_name@@@"
set_required RES_dnsName        "@@@RESOURCE:dnsName@@@"

set_optional VPN_CONFIG_DIR        "@@@vpn_config_dir@@@"         "/etc/openvpn/easy-rsa/2.0"
set_optional VPN_REMOTE_CONFIG_DIR "@@@vpn_remote_config_dir@@@"  "/etc/openvpn/easy-rsa/2.0"

log "Creating VPN credentials and config for client '$client_name' ..."

[ -d "$VPN_CONFIG_DIR" ] || fatal "VPN config directory '$VPN_CONFIG_DIR' does not exist"

# generate client config from template, use s### for replacements that are path names and thus contain '/'
# The client.conf.template is distributed in <sdm_dist_dir>/util/cloud/ec2/client.conf.template
# @@@include:client.conf.template@@@
config_file="client.conf.$client_name"
sed -e "s/%%%client%%%/$client_name/g" \
    -e "s#%%%vpn_config_dir%%%#$VPN_REMOTE_CONFIG_DIR#g" \
    ./client.conf.template > $VPN_CONFIG_DIR/$config_file

cd $VPN_CONFIG_DIR
. ./vars >&2

# Check that OPENSSL variable sourced from ./vars points to an executable openssl.
# The easy-rsa 2.0 scripts sadly don't check that themselves, so we do it here
# for them.
output=`$OPENSSL version 2>&1`
rc=$?
if [ $rc -ne 0 ] ; then
   fail "openssl binary '$OPENSSL' not found in path. Please set the correct path to openssl in $VPN_CONFIG_DIR/vars (variable OPENSSL)."
fi

output=`KEY_COMMON_NAME=$client_name ./build-key $client_name 2>&1`
rc=$?
if [ $rc -ne 0 ] ; then
   fail "Could not build VPN key for client '$client_name', exit code=$rc, output=$output"
fi

log "Copying VPN credentials and configuration to client '$client_name' ..."

local_vpn_key_dir="$VPN_CONFIG_DIR/keys"
local_vpn_ca_crt=$local_vpn_key_dir/ca.crt
local_vpn_host_crt=$local_vpn_key_dir/$client_name.crt
local_vpn_host_key=$local_vpn_key_dir/$client_name.key

VPN_REMOTE_KEY_DIR="$VPN_REMOTE_CONFIG_DIR/keys"
# copy to public DNS name, NOT using VPN, as this is quicker
prefix="root@$RES_dnsName:"

execute_scp "$VPN_CONFIG_DIR/$config_file"                                  "$prefix$VPN_REMOTE_CONFIG_DIR/$config_file"
execute_scp "$local_vpn_ca_crt" "$local_vpn_host_crt" "$local_vpn_host_key" "$prefix$VPN_REMOTE_KEY_DIR"

write_output_parameters_and_exit
