#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# General utility functions for use in conjunction with the SDM cloud adapter.
#
# This script contains general utility function for working with Amazon EC2.
#
# This shell script is sourced in from all other EC2-related shell scripts.
#
# using Amazon EC2 API Tools version 1.3-42584
# from 2009-09-24T03:07:35.000Z


#---------------------------------------------------------------------------
#  NAME
#     call_ec2 -- call an ec2 script and handle response
#
#  SYNOPSIS
#     call_ec2 command [arg1 ...]
#
#  INPUTS
#     command     - ec2 command to call
#     arg1 ...    - arguments to ec2 command
#
#  DESCRIPTION
#     Calls the given ec2 command and returns the response. Only the
#     following ec2 commands are allowed:
#     - ec2-describe-instances
#     - ec2-run-instances
#     - ec2-terminate-instances
#
#     The output is the standard tab delimited output of the EC2 API Tools.
#     This is easier to parse than the XML output in a script.
#
#     If the ec2 command produces an error, the script fails.
call_ec2()
{
   case "$1" in
      ec2-describe-instances)
         ;;
      ec2-run-instances)
         ;;
      ec2-terminate-instances)
         ;;
      *)
         fatal "Internal error: Invalid ec2 command handed to call_ec2(): '$1'"
         ;;
   esac
   if [ -z "$EC2_TOOLS_INSTALL_DIR" ] ; then
      fatal "Internal error: EC2_TOOLS_INSTALL_DIR parameter is not set but required."
   fi
   ec2_command="$EC2_TOOLS_INSTALL_DIR/bin/$1"
   shift
   if [ ! -x "$ec2_command" ] ; then
      fatal "EC2 command '$ec2_command' is not executable."
   fi

   debug "Executing ec2 command '$ec2_command' ..."

   if [ -z "$EC2_JAVA_HOME" ] ; then
      debug "No JAVA_HOME set for call to EC2 tools"
      add_options=
   else
      # EC2_JAVA_HOME is set, so use it for ec2_call
      add_options="JAVA_HOME=\"$EC2_JAVA_HOME\""
   fi

   output_file=`get_temp_file "cmd_out"`
   rc=$?
   if [ $rc -ne 0 ]; then
      # error while creating temp file => just propagate exit code
      exit $rc
   fi
   error_file=`get_temp_file "cmd_err"`
   rc=$?
   if [ $rc -ne 0 ]; then
      # error while creating temp file => just propagate exit code
      exit $rc
   fi

   # call ec2 command; in output empty fields are filled with '(nil)'
   eval \
      EC2_HOME="$EC2_TOOLS_INSTALL_DIR" \
      EC2_CERT="$EC2_USER_CERTIFICATE_FILE" \
      EC2_PRIVATE_KEY="$EC2_USER_PRIVATE_KEY_FILE" \
      $add_options \
      $ec2_command --show-empty-fields "$@" \
      > $output_file 2> $error_file
   exit_code=$?

   if [ "$DEBUG" -ge 4 ]; then
      # print only in trace mode full output of ec2 command
      trace "Output of ec2 command:"
      cat $output_file >&2
   fi

   if [ $exit_code -eq 0 ]; then
      debug "Normal exit of ec2 command."
   else
      log "Exit code of ec2 command '$ec2_command': $exit_code"
      error_msg=`cat $error_file`
      rm -f $output_file $error_file > /dev/null 2>&1
      fail "$error_msg"
   fi

   #   => just output the standard out
   cat $output_file

   rm -f $output_file $error_file > /dev/null 2>&1
}


#---------------------------------------------------------------------------
#  NAME
#     execute_ssh_script -- executes a script on EC2 host
#
#  SYNOPSIS
#     execute_ssh_script server command [do_output]
#
#  INPUTS
#     server    - EC2 host on which to execute the script
#     command   - name of script (and arguments) to execute
#     do_output - if non-empty, then print output.
#                 Otherwise (and by default) suppress output.
#
#  DESCRIPTION
#     Connects per ssh to $server and executes the "./$command" in the folder
#     $AMI_SCRIPTS_DIR
#
#     On error, exit the script.
execute_ssh_script()
{
   server="$1"
   command="$2"
   do_output="$3"

   debug "execute_ssh_script on '$server', command='$command', do_output=$do_output"

   if [ -z "$AMI_SCRIPTS_DIR" ] ; then
      fatal "Internal error: AMI_SCRIPTS_DIR parameter is not set but required."
   fi

   tmp_file=`get_temp_file "execute_ssh_script_out"`
   rc=$?
   if [ $rc -ne 0 ]; then
      # error while creating temp file => just propagate exit code
      exit $rc
   fi

   $SSH_COMMAND -q $server "cd $AMI_SCRIPTS_DIR ; ./$command" > $tmp_file 2>&1
   rc=$?

   if [ $rc -ne 0 ]; then
      # ssh problem or problem with executed command
      if [ -s $tmp_file ] ; then
         echo "Problem with execute_ssh_script(host='$server', command='$command'): RC=$rc" >&2
         cat $tmp_file >&2
         rm -f $tmp_file >/dev/null 2>&1
         fail "End of command output."
      else
         rm -f $tmp_file >/dev/null 2>&1
         fail "Problem with execute_ssh_script(host='$server', command='$command'): RC=$rc"
      fi
   fi

   if [ -n "$do_output" ]; then
      cat $tmp_file
   fi

   rm -f $tmp_file >/dev/null 2>&1
}


#---------------------------------------------------------------------------
#  NAME
#     execute_scp -- executes an scp command
#
#  SYNOPSIS
#     execute_scp username@server:files username2@server2:destination
#
#  INPUTS
#     args - what to copy from where to where
#
#  DESCRIPTION
#     Small wrapper around SCP command that suppress the output.
execute_scp()
{
   debug "execute_scp: $*"

   output=`$SCP_COMMAND "$@" 2>&1`
   rc=$?
   if [ $rc -ne 0 ]; then
      fail "Error: scp command '$SCP_COMMAND $*' failed with exit code=$rc\noutput=$output"
   fi
}



#---------------------------------------------------------------------------
#  NAME
#     get_instance_id -- parse out the instance ids from the output of
#                         ec2-run-instances command
#
#  SYNOPSIS
#     get_instance_id ec2_run_instances_output
#
#  INPUTS
#     ec2_run_instances_output - tab-delimited output of ec2-run-instances command
#
#  DESCRIPTION
#     Helper function to extract just the instance ids from tab-delimited output
get_instance_id()
{
   if [ $# -ne 1 ] ; then
      fatal "get_instance_id() called with wrong number of arguments"
   fi
   # instance_id is in 2nd field in lines beginning with INSTANCE
   # fields are tab (default delimiter for cut) separated
   #
   # example output (ec2run ami-b34fa8da):
   # RESERVATION     r-d074ffb9      391035046281    default
   # INSTANCE        i-228de44b      ami-b34fa8da                    pending         0               m1.small        2009-04-20T10:11:55+0000us-east-1b       aki-6552b60c    ari-6452b60d

   # the argument to grep needs to be quoted as "^" is special for /bin/sh
   echo "$1" | grep "^INSTANCE" | cut -f2
}

#---------------------------------------------------------------------------
#  NAME
#     is_instance_running -- find out if an instance is running from the output of
#                            ec2-describe-instances command
#
#  SYNOPSIS
#     is_instance_running ec2_describe_instances_output
#
#  INPUTS
#     ec2_describe_instances_output - tab-delimited output of ec2-describe-instances command
#
#  DESCRIPTION
#     Helper function to check if an instance is running. The information is parsed
#     from the output of the ec2-describe-instances command.
is_instance_running()
{
   # state is in 6th field in lines beginning with INSTANCE
   # fields are tab (default delimiter for cut) separated
   #
   # example output (ec2run ami-b34fa8da):
   # RESERVATION     r-d074ffb9      391035046281    default
   # INSTANCE        i-228de44b      ami-b34fa8da    (nil)   (nil)   pending         0               m1.small        2009-04-20T10:11:55+0000us-east-1b       aki-6552b60c    ari-6452b60d

   # the argument to grep needs to be quoted as "^" is special for /bin/sh
   state=`echo "$1" | grep "^INSTANCE" | cut -f6`
   debug "is_instance_running() got state='$state'"
   if [ "$state" = "running" ] ; then
      return 0
   else
      return 1
   fi
}

#---------------------------------------------------------------------------
#  NAME
#     set_res_prop_from_output -- parse output of ec2-describe-instances command
#                                 and set resource properties (RES_XXX variables)
#
#  SYNOPSIS
#     set_res_prop_from_output ec2_describe_instances_output
#
#  INPUTS
#     ec2_describe_instances_output - tab-delimited output of ec2-describe-instances command
#
#  DESCRIPTION
#     Helper function to set the resource properties variables (RES_XXX) from
#     information output by ec2-describe-instances command.
set_res_prop_from_output()
{
   debug "set_res_prop_from_output called with: '$1'"

   # example output (ec2run ami-999e7ff0):
   # RESERVATION	r-d02e3ab9	391035046281	default
   # INSTANCE	i-71974419	ami-999e7ff0	(nil)	(nil)	terminated	mytest-keypair	0	(nil)	m1.small	2009-09-25T08:16:50+0000	(nil)	aki-6552b60c	ari-6452b60d	(nil)	monitoring-disabled	(nil)	(nil)	(nil)	(nil)

   # the argument to grep needs to be quoted as "^" is special for /bin/sh
   line=`echo "$1" | grep "^INSTANCE"`

   # => store the output in the following variables
   res_props="row_header instanceId amiId RES_dnsName RES_privateDnsName instance_state \
              RES_keyName dummy dummy instanceType RES_launchTime RES_availabilityZone \
              RES_kernelId RES_ramdiskId dummy RES_monitoringState RES_ipAddress RES_privateIpAddress"

   i=0
   # TODO: use something better than multiple calls to cut - but this needs to be portable nevertheless!!
   for res_prop in $res_props; do
      i=`expr $i + 1`
      # cut out field $i
      # fields are tab separated (default delimiter for cut)
      val=`echo "$line" | cut -f$i`
      eval $res_prop=\"$val\"
      debug "$res_prop=$val"
   done

   exp_field_count=`echo $res_props | wc -w`
   if [ $i -ne $exp_field_count ]; then
      fail "The output of the ec2-describe-command returned too few fields (got $i, expected $exp_field_count)"
   fi

   # sanity checking
   if [ "$RES_amiId" != "$amiId" ] ; then
      fail "Given AMI ID does not match returned values: '$RES_amiId' vs. '$amiId'"
   fi
   if [ "$RES_instanceId" != "$instanceId" ] ; then
      fail "Given instance ID does not match returned values: '$RES_instanceId' vs. '$instanceId'"
   fi
   if [ "$RES_instanceType" != "$instanceType" ] ; then
      fail "Given instance type does not match returned values: '$RES_instanceType' vs. '$instanceType'"
   fi
}

#---------------------------------------------------------------------------
#  NAME
#     is_openvpn_running - checks if openvpn is running on this host
#
#  SYNOPSIS
#     is_openvpn_running
#
#  DESCRIPTION
#     This function checks whether openvpn is running on this host.
#     First the existence of the $VPN_PID_FILE is checked and afterwards the
#     running process is looked at.
#
#     Returns 0 if openvpn is running,
#             1 otherwise
is_openvpn_running()
{
   log "Checking if VPN server is running ..."
   if [ -r $VPN_PID_FILE ] ; then
      # pid file is readable
      vpn_pid=`cat $VPN_PID_FILE`
      if [ -n "$vpn_pid" ] ; then
         # pid is not empty
         if ps -p $vpn_pid | grep openvpn > /dev/null ; then
            return 0
         else
            debug "openvpn process with pid '$vpn_pid' not found"
         fi
      else
         debug "pid file is empty"
      fi
   else
      debug "pid file is not readable"
   fi

   return 1
}

#---------------------------------------------------------------------------
#  NAME
#     set_ssh_commands - checks and initializes ssh command related variables
#
#  SYNOPSIS
#     set_ssh_commands
#
#  DESCRIPTION
#     This function initializes the SSH_COMMAND and SCP_COMMAND variables based
#     on the EC2_USER_KEY_PAIR_FILE.
#
#     The following general options are used for the ssh/scp call:
#       - be quiet
#       - don't ask for trusting hosts
#       - don't ever ask for password
#       - don't use any known hosts file
set_ssh_commands()
{
   if [ -z "$EC2_USER_KEY_PAIR_FILE" ] ; then
      fatal "Internal error: EC2_USER_KEY_PAIR_FILE parameter is not set but required."
   fi

   if [ ! -f "$EC2_USER_KEY_PAIR_FILE" ]; then
      fatal "ec2 user key pair file '$EC2_USER_KEY_PAIR_FILE' does not exist. Please check the parameter ec2_user_key_pair_file in the cloud service configuration"
   fi

   if [ ! -r "$EC2_USER_KEY_PAIR_FILE" ]; then
      fatal "ec2 user key pair file '$EC2_USER_KEY_PAIR_FILE' is not readable. Please check the parameter ec2_user_key_pair_file in the cloud service configuration"
   fi

   # no -q option for SSH_COMMAND, add it on using side where necessary
   SSH_COMMAND="ssh -o StrictHostKeyChecking=no -o BatchMode=yes -o UserKnownHostsFile=/dev/null -i $EC2_USER_KEY_PAIR_FILE -l root"
   SCP_COMMAND="scp -o StrictHostKeyChecking=no -o BatchMode=yes -o UserKnownHostsFile=/dev/null -i $EC2_USER_KEY_PAIR_FILE -q"
}

#---------------------------------------------------------------------------
#  NAME
#     is_ssh_tunnel_running - checks if a ssh tunnel is running
#
#  SYNOPSIS
#     is_ssh_tunnel_running
#
#  DESCRIPTION
#     Checks via the RES_sshTunnelPid variable that there is in fact an
#     ssh process running under the RES_sshTunnelPid.
#
#     Returns 0 if the tunnel is running,
#             1 otherwise
is_ssh_tunnel_running()
{
   if [ -z "$RES_sshTunnelPid" ] ; then
      fatal "Internal error: RES_sshTunnelPid resource property is not set but required."
   fi

   ps -p $RES_sshTunnelPid | grep ssh > /dev/null 2>&1
}

#---------------------------------------------------------------------------
#  NAME
#     check_openvpn_and_ec2_versions - checks installed versions
#
#  SYNOPSIS
#     check_openvpn_and_ec2_versions
#
#  DESCRIPTION
#     Checks the version of openVPN and Amazon EC2 API tools pointed to by the
#     VPN_BIN_DIR and EC2_TOOLS_INSTALL_DIR.
#
#     Generates fatal errors if the binaries cannot be found.
#     Generates warning if there is a version mismatch
check_openvpn_and_ec2_versions()
{
   # TODO add newer OpenVPN version
   requiredOpenVpnVersion="2.0.9"
   requiredEc2ApiVersion="1.3-42584"

   log "Checking if correct versions of openVPN and Amazon EC2 API Tools are installed ..."
   if [ -z "$VPN_BIN_DIR" ] ; then
      fatal "Internal error: VPN_BIN_DIR parameter is not set but required."
   fi
   if [ -z "$EC2_TOOLS_INSTALL_DIR" ] ; then
      fatal "Internal error: EC2_TOOLS_INSTALL_DIR parameter is not set but required."
   fi

   # check openvpn
   openvpn_bin="$VPN_BIN_DIR/openvpn"
   if [ ! -x "$openvpn_bin" ] ; then
      fatal "OpenVPN binary '$openvpn_bin' not found or not executable."
   fi
   openVpnVersion=`$openvpn_bin --version | head -n 1 | cut -d' ' -f2`
   if [ "$openVpnVersion" != "$requiredOpenVpnVersion" ] ; then
      warn "The version of openVPN is '$openVpnVersion' instead of the required '$requiredOpenVpnVersion'"
   fi

   # check EC2 API Tools
   ec2_bin="$EC2_TOOLS_INSTALL_DIR/bin/ec2ver"
   if [ ! -x "$ec2_bin" ] ; then
      fatal "EC2 version binary '$ec2_bin' not found or not executable."
   fi
   # explicitly set EC2_HOME, otherwise the wrong version might be used!!
   ec2ApiVersion=`EC2_HOME=$EC2_TOOLS_INSTALL_DIR $ec2_bin | cut -d' ' -f1`
   if [ "$ec2ApiVersion" != "$requiredEc2ApiVersion" ] ; then
      warn "The version of the Amazon EC2 API Tools is '$ec2ApiVersion' instead of the required '$requiredEc2ApiVersion'"
   fi
}
