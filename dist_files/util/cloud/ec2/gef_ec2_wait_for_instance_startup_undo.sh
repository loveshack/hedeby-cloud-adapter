#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_ec2_wait_for_instance_startup_undo.sh [-h] [-v]

Resetting of resource properties done in wait_for_instance_startup step.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
cd `dirname $0`
. ./gef_util.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

log "Resetting of EC2 resource properties..."

# reset resource properties
RES_dnsName=
RES_privateDnsName=
RES_keyName=
RES_launchTime=
RES_availabilityZone=
RES_kernelId=
RES_ramdiskId=
RES_monitoringState=
RES_ipAddress=
RES_privateIpAddress=
RES_cloudResource=
RES_userKeyPairFile=

# everything ok
write_output_parameters_and_exit \
   RES_dnsName RES_privateDnsName RES_keyName \
   RES_launchTime RES_availabilityZone        \
   RES_kernelId RES_ramdiskId                 \
   RES_monitoringState                        \
   RES_ipAddress RES_privateIpAddress         \
   RES_cloudResource                          \
   RES_userKeyPairFile
