#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_ec2_startup_vpn_connection_undo.sh [-h] [-v]

Cleanup of ssh tunnel.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_ec2_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ec2_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

# resource properties
set_optional RES_sshTunnelPid   "@@@RESOURCE:sshTunnelPid@@@"   "unknown"
#   the following properties are just used for logging, so no problem if they are not set
set_optional RES_instanceId     "@@@RESOURCE:instanceId@@@"     "unknown"
set_optional RES_privateDnsName "@@@RESOURCE:privateDnsName@@@" "unknown"

if [ "$RES_sshTunnelPid" != "unknown" ] ; then

   log "Killing ssh tunnel (PID=$RES_sshTunnelPid) to cloud host '$RES_privateDnsName' (instance id: $RES_instanceId)..."

   # check if the ssh tunnel is still running
   if is_ssh_tunnel_running ; then
      kill $RES_sshTunnelPid 2> /dev/null
      sleep 5
      if is_ssh_tunnel_running ; then
         # still running => use the hammer
         warn "Regular kill on ssh tunnel (PID=$RES_sshTunnelPid) did not succeed, trying kill -9"
         kill -9 $RES_sshTunnelPid 2> /dev/null
      fi
   else
      warn "ssh tunnel no longer running under PID $RES_sshTunnelPid"
   fi
else
   log "No PID for ssh tunnel known => no killing possible."
fi

RES_sshTunnelPid=
RES_vpnIpAddress=

# everything ok
write_output_parameters_and_exit RES_sshTunnelPid RES_vpnIpAddress
