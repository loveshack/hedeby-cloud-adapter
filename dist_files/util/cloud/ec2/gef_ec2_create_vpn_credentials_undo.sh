#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_ec2_create_vpn_credentials_undo.sh [-h] [-v]

Deletes the created credentials for a VPN client from the VPN master. The cloud
host is not cleaned up, as it will be shut down anyway later.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_ec2_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ec2_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

# resource properties
set_optional RES_privateDnsName "@@@RESOURCE:privateDnsName@@@" ""
set_optional client_name        "@@@vpn_short_private_dns_name@@@" ""
set_optional VPN_CONFIG_DIR     "@@@vpn_config_dir@@@"          "/etc/openvpn/easy-rsa/2.0"

if [ -z "$client_name" ] ; then
   if [ -z "$RES_privateDnsName" ]; then
      log "Resource property privateDnsName not set, skipping delete of VPN config and credentials"
      write_output_parameters_and_exit
   fi
   client_name=$RES_privateDnsName
   if echo $client_name | fgrep '.' > /dev/null 2>&1 ; then
      # hostname contains '.' => long hostname
      client_name=`echo $client_name | cut -f1 -d.`
   fi
fi

log "Deleting VPN config and credentials for client '$client_name' ..."

if [ -d "$VPN_CONFIG_DIR" ] ; then
  config_file="$VPN_CONFIG_DIR/client.conf.$client_name"
  credential_files="$VPN_CONFIG_DIR/keys/$client_name.*"
  rm $config_file $credential_files
  if [ $? -ne 0 ] ; then
     warn "Problem while deleting VPN config and credentials: $config_file $credential_files"
  fi
else
  log "VPN config directory '$VPN_CONFIG_DIR' does not exist, nothing to clean up."
fi

write_output_parameters_and_exit
