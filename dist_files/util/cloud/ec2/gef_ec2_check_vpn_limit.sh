#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_ec2_check_vpn_limit.sh [-h] [-v]

Checks that the maximal number of hosts that OpenVPN can handle is not reached.

OpenVPN persists (with our server configuration) the mapping from hostnames to
private VPN IP addresses, i.e. the same hostname will always get the same IP
address. This mapping is stored inside of OpenVPN in a so called ifconfig pool.
In general, the persisting is done so that there cannot be any problems with
hostname resolving on the Java side, i.e. the same host having different IPs or
vice versa different IPs belonging to the same hostname.

In a long running system with many starting up and shutting down EC2 hosts the
number of persisted hosts increases steadily. Depending on the available pool
of EC2 resource it is theoretically possible that more different hosts are
returned from EC2 than OpenVPN is able to handle. OpenVPN has a maximal
ifconfig pool size of 16382 when using a class B subnet for the VPN. OpenVPN
cannot be used with a class A subnet (without changing defines in the source
code).

This step checks whether more than 16300 hosts (to be on the safe side) are
persisted. If this is the case, this step fails and no more hosts are started
up.

This step needs to be executed with root priviliges, as OpenVPN (which is also
run as root) creates the OPENVPN_IFCONFIG_POOL_PERSIST file with file
permissions 600.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_ec2_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ec2_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging


# the ifconfig pool persist information is stored in the OpenVPN config directory
set_optional VPN_CONFIG_DIR "@@@vpn_config_dir@@@" "/etc/openvpn/easy-rsa/2.0"
OPENVPN_IFCONFIG_POOL_PERSIST="$VPN_CONFIG_DIR/ifconfig-pool-persist.txt"

log "Checking size of OpenVPN ifconfig pool ..."
# use cat and pipe to only get the count as output of wc
# redirect STDERR of cat in case the file does not exist, then wc returns 0
num_hosts=`cat "$OPENVPN_IFCONFIG_POOL_PERSIST" 2> /dev/null | wc -l`
if [ $num_hosts -gt 16300 ] ; then
   fail "The persisted ifconfig pool '$OPENVPN_IFCONFIG_POOL_PERSIST' of OpenVPN contains too many entries: $num_hosts. Please shutdown the VPN server, all cloud hosts, restart the SDM system and delete the mentioned file."
fi

# everything ok
write_output_parameters_and_exit
