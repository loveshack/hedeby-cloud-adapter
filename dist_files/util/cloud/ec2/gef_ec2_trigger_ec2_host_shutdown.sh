#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_ec2_trigger_ec2_host_shutdown.sh [-h] [-v]

Triggers the shutdown of a host on Amazon EC2.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_ec2_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ec2_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

# general parameters
set_required TMP_DIR     "@@@tmp_dir@@@"

# ec2 parameters
set_required EC2_TOOLS_INSTALL_DIR     "@@@ec2_tools_install_dir@@@"
set_required EC2_USER_CERTIFICATE_FILE "@@@ec2_user_certificate_file@@@"
set_required EC2_USER_PRIVATE_KEY_FILE "@@@ec2_user_private_key_file@@@"
set_optional EC2_JAVA_HOME             "@@@ec2_java_home@@@" ""

# resource property parameters 
set_required RES_instanceId     "@@@RESOURCE:instanceId@@@"
set_required RES_privateDnsName "@@@RESOURCE:privateDnsName@@@"

log "Triggering shutdown of EC2 host '$RES_privateDnsName' with instanceId=$RES_instanceId ..."

# terminate the EC2 instance
call_ec2 ec2-terminate-instances "$RES_instanceId"

debug "Resetting all ec2 related resource properties"
RES_instanceId=
RES_dnsName=
RES_privateDnsName=
RES_keyName=
RES_launchTime=
RES_availabilityZone=
RES_kernelId=
RES_ramdiskId=
RES_monitoringState=
RES_ipAddress=
RES_privateIpAddress=
RES_cloudResource=
RES_userKeyPairFile=

# everything ok
write_output_parameters_and_exit \
   RES_instanceId        \
   RES_dnsName           \
   RES_privateDnsName    \
   RES_keyName           \
   RES_launchTime        \
   RES_availabilityZone  \
   RES_kernelId          \
   RES_ramdiskId         \
   RES_monitoringState   \
   RES_ipAddress         \
   RES_privateIpAddress  \
   RES_cloudResource     \
   RES_userKeyPairFile
