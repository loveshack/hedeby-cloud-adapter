#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_ec2_install_sdm.sh [-h] [-v]

Install SDM managed host on a cloud host:
   o Copies the SDM credentials to cloud host
   o Install managed host on cloud host
   o Startup JVM on cloud host

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_ec2_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ec2_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

set_required TMP_DIR          "@@@tmp_dir@@@"
set_required SYSTEM_NAME      "@@@system_name@@@"
set_required CS_URL           "@@@cs_url@@@"

# the SDM spool directory
set_required SYSTEM_SPOOL_DIR "@@@system_spool_dir@@@"
sdm_keystore=$SYSTEM_SPOOL_DIR/security/users/root.keystore
sdm_cacert=$SYSTEM_SPOOL_DIR/security/ca/ca_top/cacert.pem

# ec2 parameters
set_required EC2_USER_KEY_PAIR_FILE "@@@ec2_user_key_pair_file@@@"
set_ssh_commands

# resource properties
set_required RES_instanceId     "@@@RESOURCE:instanceId@@@"
set_required RES_dnsName        "@@@RESOURCE:dnsName@@@"
set_required RES_privateDnsName "@@@RESOURCE:privateDnsName@@@"

# SDM install properties (can be true or false)
set_required SDM_NO_SSL         "@@@sdm_no_ssl@@@"

set_optional AMI_SCRIPTS_DIR    "@@@ami_scripts_dir@@@"   "/opt/sdm/util/cloud/ec2/ami_scripts"

# Uses scp to copy the SDM keystore and certificate for user root from
# SYSTEM_SPOOL_DIR to host. They are temporarily stored in the /root directory
# on the cloud host.
log "Copying SDM certificates to cloud host '$RES_privateDnsName' (instance id=$RES_instanceId) ..."

execute_scp "$sdm_keystore" "$sdm_cacert" "root@$RES_dnsName:/root"

if [ "$SDM_NO_SSL" = "false" ] ; then
   SDM_OPT_ARGS=
else
   log "Installing SDM in no_ssl mode."
   SDM_OPT_ARGS='-nossl'
fi


log "Installing and starting up SDM on cloud host '$RES_privateDnsName' ..."

debug "additional arguments to managed host install: $SDM_OPT_ARGS"

execute_ssh_script $RES_dnsName "startup-sdm.sh $SYSTEM_NAME $CS_URL $SDM_OPT_ARGS"

write_output_parameters_and_exit
