#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_ec2_fix_ami.sh [-h] [-v]

Apply small fix to AMI that does not contain latest version of AMI scripts.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:gef_ec2_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ec2_common.sh

handle_default_command_line_args "$@"
set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

set_required TMP_DIR          "@@@tmp_dir@@@"

# ec2 parameters
set_required EC2_USER_KEY_PAIR_FILE "@@@ec2_user_key_pair_file@@@"
set_ssh_commands

# resource properties
set_required RES_instanceId     "@@@RESOURCE:instanceId@@@"
set_required RES_dnsName        "@@@RESOURCE:dnsName@@@"
set_required RES_privateDnsName "@@@RESOURCE:privateDnsName@@@"

# copy from where to where
set_required SCRIPTS_DIR        "@@@scripts_dir@@@"
set_optional AMI_SCRIPTS_DIR    "@@@ami_scripts_dir@@@"   "/opt/sdm/util/cloud/ec2/ami_scripts"

log "TODO: fixing AMI for cloud host '$RES_privateDnsName' (instance id=$RES_instanceId)"

log "copying correct AMI scripts ..."
execute_scp $SCRIPTS_DIR/*.sh root@$RES_dnsName:$AMI_SCRIPTS_DIR

log "copying changed sshd config and restarting ssh service ..."
execute_scp $SCRIPTS_DIR/sshd_config root@$RES_dnsName:/etc/ssh
$SSH_COMMAND -q $RES_dnsName "svcadm refresh ssh" >&2
rc=$?
if [ $rc -ne 0 ]; then
   # ssh problem or problem with executed command
   fail "Problem with svcadm command (host='$server', command='$command'): RC=$rc"
fi

write_output_parameters_and_exit
