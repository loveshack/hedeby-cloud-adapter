#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

#---------------------------------------------------------------------------
# This file contains helper methods for gef actions that implements
# power saving va IPMI.
#---------------------------------------------------------------------------


# set -x

#---------------------------------------------------------------------------
#  NAME
#     ipmi_cmd -- Execute an IPMI command
#
#  SYNOPSIS
#     impi_cmd [-no_error] <ipmi command>
#
#  INPUTS
#     -no_error       - do not fail if an error occurs
#     <ipmi command>  - the ipmi command
#
#  DESCRIPTION
#
#  Executes an ipmi command on a host. This method requires the following
#  variables in the environment:
#
#  IPMI_TOOL         --  path of the impitool binary
#  RES_ipmiHostname  --  hostname of the IPMI interface of the host
#  IPMI_USER         --  name of the IPMI user
#  IPMI_PW_FILE      --  path to the file containing the IPMI password
#
ipmi_cmd() {

   fail_on_error=1

   if [ $# -lt 1 ]; then
      fatal "Invalid number of parameter for impi_cmd (got $#, need at least 1)"
   fi
   if [ "$1" = "-no_error" ]; then
      fail_on_error=0
      shift
   fi

   if [ -z "$IPMI_TOOL" ]; then
      fatal "Required variable IPMI_TOOL is not set"
   fi
   if [ -z "$RES_ipmiHostname" ]; then
      fatal "Required variable RES_ipmiHostname is not set"
   fi
   if [ -z "$IPMI_USER" ]; then
      fatal "Required variable IPMI_USER is not set"
   fi
   if [ -z "$IPMI_PW_FILE" ]; then
      fatal "Required variable IPMI_PW_FILE is not set"
   fi

   if [ ! -r "$IPMI_PW_FILE" ]; then
      fatal "ipmi password file '$IPMI_PW_FILE' is not accessible or does not exist"
   fi

   if [ -z "$IPMI_PORT" ]; then
      fatal "Required variable IPMI_PORT is not set"
   fi

   if [ ! -r "$IPMI_TOOL" ]; then
      msg="$IPMI_TOOL not found.\n"
      msg="${msg}Please specify the correct path to ipmitool in the parameter ipmiTool of the action configuration\n"
      fatal $msg
   fi
   if [ ! -x "$IPMI_TOOL" ]; then
      fatal "$IPMI_TOOL is not executable"
   fi
   debug "Executing IPMI command '$@' on host '$RES_ipmiHostname'"
   $IPMI_TOOL -H "$RES_ipmiHostname" -U "$IPMI_USER" -p "$IPMI_PORT" -f "$IPMI_PW_FILE" "$@"
   rc=$?
   if [ $rc != 0 ]; then
      if [ "$fail_on_error" -eq 1 ]; then
         fail "IPMI command '$@' on host 'RES_ipmiHostname' $failed (rc=$rc)"
      else
         debug "IPMI command '$@' on host 'RES_ipmiHostname' $failed (rc=$rc)"
      fi
   fi
}

#---------------------------------------------------------------------------
#  NAME
#     get_ipmi_status --  get the status of a host via IPMI
#
#  SYNOPSIS
#     get_ipmi_status
#
#  DESCRIPTION
#
#  This command executes the IPMI command 'chassis power status' via the
#  procedure impi_cmd.
#
#  OUTPUT:  (stdout)
#     on  --  if the host is powered on
#     off --  if the host is powered off
#
#  EXIT CODE:
#     0   --  if the power status has been detected
#     else -- if the ipmi command failed or printed an unexpected output
#
get_ipmi_status() {
    status=`ipmi_cmd chassis power status`
    rc=$?
    if [ "$rc" -ne 0 ]; then
       # propagate the exit code
       exit $rc
    fi
    debug "IPMI status of host '$RES_ipmiHostname' is '$status'"
    case "$status" in
        "Chassis Power is off")
           echo "off";;
        "Chassis Power is on")
           echo "on";;
        *) fatal "IPMI command 'chassis status' on host '$RES_ipmiHostname' reported unknown status: '$status'"
           ;;
    esac

}
