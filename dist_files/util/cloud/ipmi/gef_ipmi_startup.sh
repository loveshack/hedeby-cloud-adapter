#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# set -x

# print usage and exit
usage()
{
   cat <<EOUSAGE
usage:
gef_ipmi_startup.sh [-h] [-v]

Triggers the startup (power on) of a host resource via IPMI.

Options
   -h|--help     - outputs this usage message
   -v|--verbose  - turns on debugging/tracing output to STDERR
                   can be used multiple times to increase amount of output

EOUSAGE
   exit 0
}

# source utility functions
# The include directives make sure that the relevant files are transferred to
# the SDM executor. They are put into the SAME directory as the script itself,
# so they are also sourced from the current dir.
#
# @@@include:../gef_util.sh@@@
# @@@include:./gef_ipmi_common.sh@@@
cd `dirname $0`
. ./gef_util.sh
. ./gef_ipmi_common.sh

handle_default_command_line_args "$@"

set_optional LOG_LEVEL "@@@script_log_level@@@" "DEBUG"
init_logging

# The unbound_name resource property must contain the hostname of the resource
set_required unbound_name "@@@RESOURCE:unbound_name@@@"

# the resource attribute contain the number of reboots
set_optional RES_powerCycleCount "@@@RESOURCE:powerCycleCount@@@" "0"

# hostname of the IPMI controller in the host resource
set_required RES_ipmiHostname "@@@RESOURCE:ipmiHostname@@@"

set_optional IPMI_PW_FILE  "@@@RESOURCE:ipmiPasswordFile@@@" ""

if [ -z "$IPMI_PW_FILE" ]; then
   set_required IPMI_PW_FILE "@@@ipmiPasswordFile@@@"
fi

set_optional IPMI_USER         "@@@RESOURCE:ipmiUser@@@" ""
if [ -z "$IPMI_USER" ]; then
   set_optional IPMI_USER  "@@@ipmiUser@@@" "root"
fi

set_optional IPMI_PORT  "@@@RESOURCE:ipmiPort@@@" ""
if [ -z "$IPMI_PORT" ]; then
   set_optional IPMI_PORT  "@@@ipmiPort@@@" "623"
fi

# Define the path of IPMI binary
set_optional IPMI_TOOL         "@@@ipmiTool@@@" /usr/bin/ipmitool

status=`get_ipmi_status`
rc=$?
if [ "$rc" -ne 0 ]; then
   # propate the exit code
   exit $rc
fi
if [ "$status" = "off" ]; then
    debug "Starting up host '$unbound_name' (IPMI name '$RES_ipmiHostname')"
    ipmi_cmd chassis power on
    debug "IPMI command 'chassis on' successfully invoked ($RES_ipmiHostname)"

    # increment the power cycle counter
    RES_powerCycleCount=`expr $RES_powerCycleCount + 1`
    debug "Resource power cycle count is $RES_powerCycleCount"
else
   warn "Do not power on host '$RES_resourceHostname', it is already started"
fi

# Set the property resourceHostname into the resource
RES_resourceHostname=$unbound_name

write_output_parameters_and_exit RES_powerCycleCount RES_resourceHostname



