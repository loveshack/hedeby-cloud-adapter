#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# General utility functions for use in conjunction with the SDM cloud adapter.
#
# This script contains general utility function for working with the General
# Execution Framework (GEF).
#
# This shell script is sourced in from all other shell scripts.

#---------------------------------------------------------------------------
#  NAME
#     set_required -- sets a required variable
#
#  SYNOPSIS
#     set_required variable_name content
#
#  INPUTS
#     variable_name - name of the variable to set
#     content       - new content of the variable
#
#  DESCRIPTION
#     This function is used for setting a required variable. It fails if the
#     content is empty or not replaced.
#
#     This is used for parameter passing. The GEF framework in SDM replaces
#     patterns of @@@xxx@@@ with the proper contents for xxx (where xxx can be
#     a parameter or a resource property). Unknown parameters remain
#     unreplaced, therefore this function checks whether the content still
#     looks like a replacement value. If this is the case, an error is
#     produced.
#
#     The assignment is logged on debug level.
#
#  EXAMPLE
#     set_required TMP_DIR "@@@tmp_dir@@@"
set_required()
{
   if [ $# -ne 2 ] ; then
      fatal "set_required() called with illegal number of arguments ($# -ne 2)"
   fi
   # check if value is still default
   expr "$2" : "@@@.*@@@" > /dev/null
   rc=$?
   if [ -z "$2" -o $rc -eq 0 ] ; then
      # error, value empty or not replaced
      expr "$1" : "RES_" > /dev/null
      if [ "$?" -eq 0 ] ; then
         # it's a resource property
         prop_name=`expr "$1" : 'RES_\(.*\)'`
         fail_no_undo "Required resource property '$prop_name' not set"
      else
         fail_no_undo "Required parameter '$1' not set"
      fi
   else
      debug "Setting variable $1=$2"
      eval "$1"="$2"
   fi
}

#---------------------------------------------------------------------------
#  NAME
#     set_optional -- sets an optional variable
#
#  SYNOPSIS
#     set_optional variable_name content default_content
#
#  INPUTS
#     variable_name   - name of the variable to set
#     content         - new content of the variable
#     default_content - default content of the variable
#
#  DESCRIPTION
#     This function is used for setting an optional variable. If content is
#     empty or not replaced, the default value is used.
#
#     This is used for parameter passing. The GEF framework in SDM replaces
#     patterns of @@@xxx@@@ with the proper contents for xxx (where xxx can be
#     a parameter or a resource property). Unknown parameters remain
#     unreplaced, therefore this function checks whether the content still
#     looks like a replacement value.
#
#     The assignment is logged on debug level.
#
#  EXAMPLE
#     set_optional HOSTS_FILE "@@@hosts_file@@@" "/etc/hosts"
set_optional()
{
   if [ $# -ne 3 ] ; then
      fatal "set_optional() called with illegal number of arguments ($# -ne 3)"
   fi
   # check if value is still default
   expr "$2" : "@@@.*@@@" > /dev/null
   rc=$?
   if [ -z "$2" -o $rc -eq 0 ] ; then
      # variable is empty or unreplaced => use default value
      val="$3"
      debug "Setting variable $1=$val [optional variable set to default value]"
   else
      val="$2"
      debug "Setting variable $1=$val [optional variable]"
   fi

   eval "$1"="$val"
}

#---------------------------------------------------------------------------
#  NAME
#     warn -- output warning information
#
#  SYNOPSIS
#     warn msg 
#
#  INPUTS
#     msg - message to report
#
#  DESCRIPTION
#     Prints the warning message to STDERR if warning is enabled. This uses the
#     DEBUG variable to determine whether to log or not.
warn()
{
   if [ "$DEBUG" -ge 1 ]; then
      echo "$*" >&2 
   fi
}

#---------------------------------------------------------------------------
#  NAME
#     log -- output logging information
#
#  SYNOPSIS
#     log msg 
#
#  INPUTS
#     msg - message to report
#
#  DESCRIPTION
#     Prints the logging message to STDERR if logging is enabled. This uses the
#     DEBUG variable to determine whether to log or not.
log()
{
   if [ "$DEBUG" -ge 2 ]; then
      echo "$*" >&2 
   fi
}

#---------------------------------------------------------------------------
#  NAME
#     debug -- output debugging information
#
#  SYNOPSIS
#     debug msg 
#
#  INPUTS
#     msg - message to report
#
#  DESCRIPTION
#     Prints the debugging message to STDERR if debugging is enabled. This uses
#     the DEBUG variable to determine whether to log or not.
debug()
{
   if [ "$DEBUG" -ge 3 ]; then
      echo "$*" >&2 
   fi
}

#---------------------------------------------------------------------------
#  NAME
#     trace -- output tracing information
#
#  SYNOPSIS
#     trace msg 
#
#  INPUTS
#     msg - message to report
#
#  DESCRIPTION
#     Prints the tracing message to STDERR if tracing is enabled. This uses the
#     DEBUG variable to determine whether to log or not.
trace()
{
   if [ "$DEBUG" -ge 4 ]; then
      echo "$*" >&2 
   fi
}

#---------------------------------------------------------------------------
#  NAME
#     handle_default_command_line_args -- handle command line args
#
#  SYNOPSIS
#     handle_default_command_line_args command_line_arguments
#
#  INPUTS
#     command_line_arguments - the arugments from the command line of the script
#
#  DESCRIPTION
#     Handles the parsing of the standard -v and -h command line parameters
#     that can be passed to the scripts. Any unknown parameters lead to errors.
#
#     Furthermore the DEBUG and GEN_RANDOM_CHARS variables are initialized.
#
#  EXAMPLE
#     Usually this function should be called like this at the top of a gef
#     script after sourcing this file:
#
#     handle_default_command_line_args "$@"
handle_default_command_line_args()
{
   # per default debugging is disabled
   DEBUG=0

   # deal with command line arguments
   while [ $# -gt 0 ]; do
      case $1 in
      -v | --verbose | -verbose | --verb | -verb )
         DEBUG=`expr $DEBUG + 1`
         ;;
      -vv )
         DEBUG=`expr $DEBUG + 2`
         ;;
      -vvv )
         DEBUG=`expr $DEBUG + 3`
         ;;
      -help | -h | -? | --help )
         usage
         ;;
      -*)
         error "Unrecognized option: '$1'"
         ;;
      *)
         break
         ;;
      esac
      shift
   done

   if [ $# -gt 0 ]; then
      surplus_args=`echo "$@"`
      fail_no_undo "Too many arguments: '$surplus_args'"
   fi

   debug "after handle_default_command_line_args debug=$DEBUG"

   # probe for some file name randomizer mechanisms
   #  -> use /dev/urandom if available
   #     otherwise use /dev/random instead
   GEN_RANDOM_CHARS=
   for dev in urandom random ; do
      if [ -r /dev/$dev ] ; then
         # od -x produces output lines that looks like:
         #   0000000 ccec 4443 0078 7e33 024f ef17 d1a2 0496
         # this is transformed to
         #   0000000ccec444300787e33024fef17d1a20496
         # and finally the first 7 characters are discarded
         #   ccec444300787e33024fef17d1a20496
         # which leaves 32 random hex chars
         GEN_RANDOM_CHARS="cat /dev/urandom | od -x | head -n 1 | tr -d ' ' | cut -c8-"
         break
      fi
   done

   if [ -z "$GEN_RANDOM_CHARS" ] ; then
      # give up
      fatal "No random number generator found (tried /dev/urandom and /dev/random)"
   fi
}

#---------------------------------------------------------------------------
#  NAME
#     init_logging -- initialise the logging of the scripts
#
#  SYNOPSIS
#     init_logging
#
#  DESCRIPTION
#     This sets the DEBUG variable that is used by all logging functions. The
#     value set depends on the global variable LOG_LEVEL that is passed in from
#     the GEF framework.
#
#     The DEBUG variable can also be set via the -v command line parameter.
#     This works additative, only the LOG_LEVEL ERROR overwrites anything set
#     on the command line. Otherwise, if e.g. LOG_LEVEL=WARNING and we have on
#     the command line one -v option, then DEBUG=2 (meaning log() messages and
#     warnings and errors are printed).
init_logging()
{
   case "$LOG_LEVEL" in
   DEBUG)
      # prints debug() messages
      DEBUG=`expr $DEBUG + 3`
      ;;
   INFO)
      # normal log() messages are printed
      DEBUG=`expr $DEBUG + 2`
      ;;
   WARNING)
      # only prints warnings and above
      DEBUG=`expr $DEBUG + 1`
      ;;
   ERROR)
      DEBUG=0
      ;;
   *)
      fatal "Internal error: Unknown logging level '$LOG_LEVEL'"
      ;;
   esac

   debug "after init_logging debug=$DEBUG"
}

#---------------------------------------------------------------------------
#  NAME
#     error -- report error and exit script
#
#  SYNOPSIS
#     error msg [exit_code]
#
#  INPUTS
#     msg       - error message to report
#     exit_code - what error code to exit with
#
#  DESCRIPTION
#     Prints the error message to STDERR and exits the shell script. Does NOT
#     return to calling function.
error()
{
   echo "$1" >&2
   exit $2
}

#---------------------------------------------------------------------------
#  NAME
#     fail -- report error, undo necessary
#
#  SYNOPSIS
#     fail msg
#
#  INPUTS
#     msg       - error message to report
#
#  DESCRIPTION
#     Convenience function for error reporting, used in case that some changes
#     were done that should be undone. (exit code 1)
fail()
{
   error "$1" 1
}

#---------------------------------------------------------------------------
#  NAME
#     fail_no_undo -- report error, no undo necessary
#
#  SYNOPSIS
#     fail_no_undo msg
#
#  INPUTS
#     msg       - error message to report
#
#  DESCRIPTION
#     Convenience function for error reporting, used in case that nothing was
#     changed so far, so no undo is necessary. (exit code 2)
fail_no_undo()
{
   error "$1" 2
}

#---------------------------------------------------------------------------
#  NAME
#     fatal -- report permanent error
#
#  SYNOPSIS
#     fatal msg
#
#  INPUTS
#     msg       - error message to report
#
#  DESCRIPTION
#     Convenience function for error reporting, used in case that an error
#     occurred that will not go away (like binaries missing, etc.) in a retry.
#     (exit code 3)
fatal()
{
   error "$1" 3
}

#---------------------------------------------------------------------------
#  NAME
#     rerun -- report that a waiting step needs to rerun
#
#  SYNOPSIS
#     rerun
#
#  DESCRIPTION
#     For waiting steps, the exit code 4 signals that the step needs to be rerun.
#     The exit code 0 means that the waiting is done, the exit code 1 signals some
#     extraordinary error.
rerun()
{
   exit 4
}


#---------------------------------------------------------------------------
#  NAME
#     write_output_parameters
#
#  SYNOPSIS
#     write_output_parameters ["no_header"] [parameter_names]
#
#  INPUTS
#     "no_header"     - if specified, no header line is printed (fixed string)
#     parameter_names - the names of the variables that are output
#
#  DESCRIPTION
#     Writes the given parameters to stdout in the format "name=value", one per
#     line.
#
#     If the parameter starts with "RES_XXX", the output is
#     "RESOURCE:XXX=value" and is treated as a resource property.
#
#     Normally, the list of parameters is prefaced by a line containing
#     "OUTPUT_PARAMETERS", unless the first argument to this function is
#     "no_header". This is useful for outputting parameters in multiple steps
#     spread over a whole script and not in one go at the end. This makes a
#     difference in case of an error during the execution of the script.
#
#  SEE ALSO
#     write_output_parameters_header
#     write_output_parameters_and_exit
write_output_parameters()
{
   if [ "$1" = "no_header" ] ; then
      shift
   else
      # write header
      echo "OUTPUT_PARAMETERS"
   fi
   while [ $# -gt 0 ]; do
      expr "$1" : "RES_" > /dev/null
      if [ "$?" -eq 0 ] ; then
         prop_name=`expr "$1" : 'RES_\(.*\)'`
         eval "echo RESOURCE:$prop_name=\$$1"
      else
         eval "echo $1=\$$1"
      fi
      shift
   done
}

#---------------------------------------------------------------------------
#  NAME
#     write_output_parameters_and_exit
#
#  SYNOPSIS
#     write_output_parameters_and_exit ["no_header"] [parameter_names]
#
#  INPUTS
#     "no_header"     - if specified, no header line is printed (fixed string)
#     parameter_names - the names of the variables that are output
#
#  DESCRIPTION
#     Writes the given parameters to stdout in the format "name=value", one per
#     line. Exits the script with exit value 0.
#
#     If the parameter starts with "RES_" followed by XXX, the output is
#     "RESOURCE:XXX=value" and is treated as a resource property.
#
#     Normally, the list of parameters is prefaced by a line containing
#     "OUTPUT_PARAMETERS", unless the first argument to this function is
#     "no_header". This is useful for outputting parameters in multiple steps
#     spread over a whole script and not in one go at the end. This makes a
#     difference in case of an error during the execution of the script.
#
#  SEE ALSO
#     write_output_parameters
#     write_output_parameters_header
write_output_parameters_and_exit()
{
   write_output_parameters "$@"
   exit 0
}


#---------------------------------------------------------------------------
#  NAME
#     random_hex -- produces a string of random hexadecimal characters
#
#  SYNOPSIS
#     random_hex [length]
#
#  INPUTS
#     length - length of the string (default: 16, max 32)
#
#  DESCRIPTION
#     Prints a random string of 'length' hexadecimal characters.
random_hex()
{
   length=${1:-16}

   eval $GEN_RANDOM_CHARS | cut -c-$length 
}

#---------------------------------------------------------------------------
#  NAME
#     get_temp_file -- create and return temporary file
#
#  SYNOPSIS
#     get_temp_file [prefix]
#
#  INPUTS
#     prefix - the prefix to use for the filename, defaults to 'tmp'
#
#  DESCRIPTION
#     A temp file is created with permissions 600 in the TMP_DIR.
#     with the name gef_prefix_$$.XXXXXXX, with prefix replaced by the
#     given prefix and XXXXXXX replaced by a random hex sequence of 16
#     characters.
get_temp_file()
{
   if [ -z "$TMP_DIR" ] ; then
      fatal "Internal error: TMP_DIR parameter is not set but required."
   fi

   prefix=$TMP_DIR/gef_${1:-tmp}_$$
   temp_file=$prefix.`random_hex`

   touch $temp_file && chmod 0600 $temp_file || fail "Could not create temporary file: $temp_file"

   echo $temp_file
}

