/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.cloud;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.common.SLOConfig;
import com.sun.grid.grm.config.cloud.CloudAdapterConfig;
import com.sun.grid.grm.resource.ResourceFactory;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.adapter.ResourceAdapterStore;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.slo.SLO;
import com.sun.grid.grm.service.slo.SLOFactory;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.gef.service.AbstractGefServiceAdapterImpl;
import com.sun.grid.grm.service.slo.impl.RunnableSLOManager;
import com.sun.grid.grm.service.slo.impl.RunnableSLOManagerSetup;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;

/**
 * <p>
 * This is the implementation of a Spare Pool ServiceAdapter.
 * The SparePoolServiceAdapterImpl class represents an internal pool of resources which are not
 * currently being used. A SparePool presents a constant need for resources to
 * the resource provider at a specified urgency level and period.
 * </p>
 */
public class CloudServiceAdapterImpl extends AbstractGefServiceAdapterImpl<ExecutorService, CloudAdapterConfig, ResourceId, CloudResourceAdapter> {

    private static final String BUNDLE = "com.sun.grid.grm.service.impl.cloud.messages";
    private static final Logger log = Logger.getLogger(CloudServiceAdapterImpl.class.getName(), BUNDLE);
    private final static long DEFAULT_SLO_UPATE_INTERVAL = 60000;
    //package private for testing
    private CloudResourceAdapterStore resourceStore;

    private final AtomicReference<CloudGef> gef = new AtomicReference<CloudGef>();

    /**
     * Creates an instance of SparePool.
     * @param component the owning component
     * @throws com.sun.grid.grm.GrmException in case any error occurs
     */
    public CloudServiceAdapterImpl(CloudServiceImpl component) throws GrmException {
        super(component);
    }

    protected void reconfigure(CloudAdapterConfig config) throws GrmException {
        gef.set(new CloudGef(getExecutionEnv(), config.getGef()));
    }

    /*
     *This method stop the service
     *@param isFreeResources indicates wheter to free resources or not
     */
    @Override
    public ServiceState doStopService(boolean freeResources) throws GrmException {
        gef.set(null);
        return ServiceState.STOPPED;
    }

    CloudResourceAdapterStore getCloudResourceAdapterStore() throws ServiceNotActiveException {
        return (CloudResourceAdapterStore) getResourceStore();
    }

    /**
     * Create the resource factory of the service
     * @return a default resource factory
     */
    @Override
    protected ResourceFactory<CloudResourceAdapter> createResourceFactory() {
        return new CloudResourceFactory(this);
    }

    
    /**
     * Create the SLOManager of the spare pool.
     * @return an RunnableSLOManager which makes every minute and SLO run
     */
    @Override
    protected RunnableSLOManager createSLOManager() {
        return new RunnableSLOManager(getName(), this, DEFAULT_SLO_UPATE_INTERVAL);
    }

    /**
     * Create the resource adapter store for this service
     * @param config the configuration of the service
     * @return the resource adapter store
     */
    protected ResourceAdapterStore<ResourceId, CloudResourceAdapter> createResourceAdapterStore(CloudAdapterConfig config) {
        log.entering(CloudServiceAdapterImpl.class.getName(), "createResourceAdapterStore", config);

        File spoolDir = PathUtil.getSpoolDirForComponent(getExecutionEnv(), getName());
        resourceStore = new CloudResourceAdapterStore(this, spoolDir, config.getMaxCloudHostsInSystemLimit());

        log.exiting(CloudServiceAdapterImpl.class.getName(), "createResourceAdapterStore", resourceStore);
        return resourceStore;
    }

    /**
     * Cloud adapter does not free any resources, short timeout.
     * @return one minute in milliseconds
     */
    @Override
    protected long getFreeResourcesTimeout() {
        return 60000;
    }

    /**
     * Creates the setup for the SLOManager
     * @param config the configuration of the SparePool
     * @return the setup for the SLOManager
     * @throws com.sun.grid.grm.GrmException if the configuration is invalid
     */
    @Override
    protected RunnableSLOManagerSetup createSLOManagerSetup(CloudAdapterConfig config) throws GrmException {
        log.entering(CloudServiceAdapterImpl.class.getName(), "createSLOManagerSetup", config);
        List<SLO> slos = createSLOs(config);
        RunnableSLOManagerSetup ret = new RunnableSLOManagerSetup(DEFAULT_SLO_UPATE_INTERVAL, slos);
        log.exiting(CloudServiceAdapterImpl.class.getName(), "createSLOManagerSetup", ret);
        return ret;
    }
    

    private List<SLO> createSLOs(CloudAdapterConfig aConfig) throws GrmException {
        log.entering(CloudServiceAdapterImpl.class.getName(), "createSLOs", aConfig);

        if (!aConfig.isSetSlos()) {
            List<SLO> ret = Collections.<SLO>emptyList();
            log.exiting(CloudServiceAdapterImpl.class.getName(), "createSLOs", ret);
            return ret;
        }

        List<SLOConfig> sloConfigs = aConfig.getSlos().getSlo();
        List<SLO> ret = new ArrayList<SLO>(sloConfigs.size());
        SLOFactory fac = SLOFactory.getDefault();

        for (SLOConfig sloc : sloConfigs) {
            SLO slo = null;

            try {
                slo = fac.createSLO(sloc);
            } catch (GrmException ex) {
                throw new GrmException("cloudadapter.ex.slo", ex, BUNDLE,
                        sloc.getName(), ex.getLocalizedMessage());
            }

            if (slo == null) {
                throw new GrmException("cloudadapter.ex.uslo", BUNDLE, sloc.getName(),
                        sloc.getClass().getSimpleName());
            } else {
                ret.add(slo);
            }
        }

        log.exiting(CloudServiceAdapterImpl.class.getName(), "createSLOs", ret);
        return ret;
    }
    
    /**
     * Friend method that gives cloud resource adapater access to the gef actions
     *
     * @return the cloud gef
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    CloudGef getGef() throws ServiceNotActiveException {
        CloudGef ret = gef.get();
        if (ret == null) {
            throw new ServiceNotActiveException();
        }
        return ret;
    }

}
