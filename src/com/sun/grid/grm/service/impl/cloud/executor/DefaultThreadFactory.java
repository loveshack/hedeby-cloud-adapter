/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/ 
package com.sun.grid.grm.service.impl.cloud.executor;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

/**
 * ThreadFactory which allows that give the created Threads a name and
 * a threadGroup.
 * 
 * The name of the thread has the format 
 * <pre>
 *     name[&lt;seqNr&gt;]
 * </pre>
 * 
 * where <code>seqNr</code> is a counter which counts the already created
 * threads.
 * The threadGroup can be <code>null</code>
 */
class DefaultThreadFactory implements ThreadFactory {

    private final AtomicLong threadSequence = new AtomicLong();
    private final String name;
    private final ThreadGroup threadGroup;

    /**
     * Create a DefaultThreadFactory
     * @param name the name prefix of the thread names
     */
    public DefaultThreadFactory(String name) {
        this(name, null);
    }

    /**
     * Create a DefaultThreadFactory which assigns all created thread to a thread group
     * @param name   the name prefix of the thread names
     * @param threadGroup the thread group (can be null)
     */
    public DefaultThreadFactory(String name, ThreadGroup threadGroup) {
        super();
        this.name = name;
        this.threadGroup = threadGroup;
    }

    private String nextThreadName() {
        return String.format("%s[%d]", name, threadSequence.getAndIncrement());
    }
    /**
     * Create a new thread
     * @param r  the runnable
     * @return
     */
    public Thread newThread(Runnable r) {
        if (threadGroup == null) {
            return new Thread(r, nextThreadName());
        } else {
            return new Thread(threadGroup, r, nextThreadName());
        }
    }
}
