/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.cloud;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceIdException;
import com.sun.grid.grm.resource.ResourceType;
import java.util.HashMap;
import java.util.Map;

/**
 * Resource factory for the CloudServiceAdapter
 */
class CloudResourceFactory extends DefaultResourceFactory<CloudResourceAdapter> {

    private final CloudServiceAdapterImpl cloudServiceAdapter;

    /**
     * Create a new instance of the CloudResourceFactory
     * @param cloudServiceAdapter the cloud service adapter
     */
    CloudResourceFactory(CloudServiceAdapterImpl cloudServiceAdapter) {
        this.cloudServiceAdapter = cloudServiceAdapter;
    }

    /**
     * Create a resource adapter out of the internal representation of the resourc
     *
     * <p>This method can create a resource adapter out of the <tt>CloudHostInfo</tt> object.</p>
     *
     * @param env  the execution env
     * @param internalRepresentation the internal representation of the resource
     * @return the resource adapter
     * @throws com.sun.grid.grm.GrmException if the resource adapter could not be created
     */
    @Override
    public CloudResourceAdapter createResourceAdapter(ExecutionEnv env, Object internalRepresentation) throws GrmException {
        
        // TODO: check properties (AMI_ID!!)

        return super.createResourceAdapter(env, internalRepresentation);
    }
    
    /**
     * Create a new Resource
     * @param env   the execution env (needed to context CS for the next resource id)
     * @param type  the type of the resource
     * @param props the properties of the resource
     * @return the new resource
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the type did not accept the properties
     * @throws ResourceIdException if the resource id could not be created
     */
    @Override
    public Resource createResource(ExecutionEnv env, ResourceType type, Map<String, Object> props) throws InvalidResourcePropertiesException, ResourceIdException {
        Map<String, Object> p = new HashMap<String, Object>(props);
        p.put(CloudResourceAdapter.RESOURCE_PROPERTY_OWNER, cloudServiceAdapter.getName());
        return createResource(env.getResourceIdFactory(), type, p);
    }
}
