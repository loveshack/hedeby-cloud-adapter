/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service.impl.cloud;

import com.sun.grid.grm.ComponentAdapterFactory;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.cloud.CloudAdapterConfig;
import com.sun.grid.grm.impl.ComponentExecutors;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.impl.AbstractServiceContainer;
import java.util.concurrent.ExecutorService;

/**
 * This class implements the <code>ServiceContainer</code> for the CloudAdapter.
 * 
 * <p>The implementation of the Cloud adapter service can be found in <code>CloudServiceAdapterImpl</code>.</br>
 *    The component representing a cloud adapter has a cached thread pool as executor.</p>
 */
public class CloudServiceImpl extends AbstractServiceContainer<CloudServiceAdapterImpl,ExecutorService,CloudAdapterConfig> implements Service {

    /**
     * Create a new SparePoolServiceImpl
     * @param env the execution env of the component
     * @param name the name of the component
     */
    public CloudServiceImpl(ExecutionEnv env, String name) {
        super(env, name, ComponentExecutors.<CloudServiceImpl,CloudAdapterConfig>newCachedThreadPoolExecutorServiceFactory(), new MyServiceAdapterFactory());
    }
    
    /**
     * The ComponentAdapterFactory creates always a instance of <code>CloudServiceAdapterImpl</code>
     */
    private static class MyServiceAdapterFactory implements ComponentAdapterFactory<CloudServiceImpl,CloudServiceAdapterImpl,ExecutorService,CloudAdapterConfig> {

        public CloudServiceAdapterImpl createAdapter(CloudServiceImpl component, CloudAdapterConfig config) throws GrmException {
            return new CloudServiceAdapterImpl(component);
        }

        public boolean hasImplementationChanged(CloudServiceImpl component, CloudServiceAdapterImpl adapter, CloudAdapterConfig config) throws GrmException {
            return false;
        } 
    }
}
