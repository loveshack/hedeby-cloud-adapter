/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.cloud;

import com.sun.grid.grm.bootstrap.AbstractModule;
import com.sun.grid.grm.bootstrap.ConfigPackage;
import com.sun.grid.grm.bootstrap.ConfigPackageImpl;
import com.sun.grid.grm.cli.CliCommand;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.validate.Validator;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 *  Module of the Grid Engine Adapter
 */
public class CloudAdapterModule extends AbstractModule {

    public static final String CLOUD_ADAPTER_VERSION = "1.0u5";//TODO!

    /** Creates a new instance of GEServiceModule */
    public CloudAdapterModule() {
        super("cloud-adapter");
    }

    /**
     *  Get the vendor of the module.
     *  @return the vendor of the module
     */
    public String getVendor() {
        return "Sun Microsystems";
    }

    /**
     *  Get the version of the module.
     *  @return the version of the module
     */
    public String getVersion() {
        return CLOUD_ADAPTER_VERSION;
    }

    /** 
     *  Load the cli extensions of this module.
     *
     *  @param extensions list where the classes of the cli extensions are stored
     */
    protected void loadCliExtension(Set<Class<? extends CliCommand>> extensions) {
        extensions.add(com.sun.grid.grm.service.impl.cloud.cli.AddCloudAdapterCliCommand.class);
    }

    /**
     * Get all the JAXB enabled packaged from the module, with their
     * namespaces and prefixes.    
     * @return all the JAXB enabled packaged from the module
     */
    public List<ConfigPackage> getConfigPackages() {
        List<ConfigPackage> cpList = new ArrayList<ConfigPackage>();

        URL localURL = getClass().getClassLoader().getResource("hedeby-cloud-adapter.xsd");

        List<String> dependencies = new ArrayList<String>(2);
        dependencies.add("common");
        dependencies.add("gef");
        
        cpList.add(new ConfigPackageImpl("com.sun.grid.grm.config.cloud",
                "http://hedeby.sunsource.net/hedeby-cloud-adapter",
                "cloud_adapter",
                localURL,
                dependencies));
        return cpList;
    }

    /** 
     *  Load the validator extensions of this module.
     *
     *  @param extensions list where the classes of the validator extensions are stored
     */
    protected void loadValidatorExtension(Set<Class<? extends Validator>> extensions) {
        extensions.add(CloudAdapterConfigValidator.class);
    }

    /**
     * The GE service installs the default complex mapping
     * @param extensions
     */
    @Override
    protected void loadMasterInstallExtensions(Set<Command> extensions) {
        /*
        MappingConfig defaultMapping = MappingUtil.createDefaultMapping();
        AddMappingCommand addDefaultMappingCmd = new AddMappingCommand(MappingUtil.DEFAULT_MAPPING_NAME, defaultMapping);
        
        extensions.add(addDefaultMappingCmd);
         */
    }

    /**
     * Get a set of new resource types introduced by this module
     * @return map of new resource types
     */
    public Set<ResourceType> getResourceTypes() {
        return Collections.<ResourceType>emptySet();
    }
}
