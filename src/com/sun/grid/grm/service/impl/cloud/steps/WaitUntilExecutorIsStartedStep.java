/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.cloud.steps;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.executor.Executor;
import com.sun.grid.grm.gef.GefActionContext;
import com.sun.grid.grm.gef.StepExitCode;
import com.sun.grid.grm.gef.StepResult;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This java steps tries to contact the executor on a host. The hostname is
 * taken from the resourceHostname property of the resource.
 * The type of the resource must be HostResourceType.
 *
 * The step succeeds if the executor has been found and its state is STARTED.
 *
 * This is a waiting step.
 *
 * @see HostResourceType
 */
public final class WaitUntilExecutorIsStartedStep extends AbstractJavaStep {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.cloud.steps.service_impl_cloud_steps";
    /**
     * Create a new instance of WaitUntilExecutorIsStartedStep
     * @param name the name of the step
     */
    public WaitUntilExecutorIsStartedStep(String name) {
        super(name);
    }

    /**
     * Execute this step.
     * @param ctx          the action context
     * @param inputParams  the input parameters (currently not used)
     * @param protocol     the protocol logger
     * @return  the output parameter (empty map)
     *
     */
    public StepResult execute(GefActionContext ctx, Map<String, Object> inputParams, Logger protocol) {
        ExecutionEnv env = ctx.getExecutionEnv();

        Resource resource = ctx.getResource();
        if(!resource.getType().getClass().equals(HostResourceType.class)) {
            protocol.log(Level.SEVERE, I18NManager.formatMessage("waitForExe.invalidResType", BUNDLE,  resource, resource.getType().getName()));
            return new StepResult(StepExitCode.FAILED_NO_UNDO);
        }

        Hostname hostname = (Hostname) resource.getProperty(HostResourceType.HOSTNAME.getName());
        if (hostname == null) {
            protocol.log(Level.SEVERE, I18NManager.formatMessage("waitForExe.missingResProp", BUNDLE, HostResourceType.HOSTNAME.getName(), resource));
            return new StepResult(StepExitCode.FAILED_NO_UNDO);
        }
        if(protocol.isLoggable(Level.FINE)) {
            protocol.log(Level.FINE, I18NManager.formatMessage("waitForExe.search", BUNDLE, hostname));
        }
        Executor executor;
        try {
            executor = ComponentService.getComponentByTypeAndHost(env, Executor.class, hostname);
        } catch (GrmException ex) {
            protocol.log(Level.SEVERE, I18NManager.formatMessage("waitForExe.search.ex", BUNDLE, ex.getLocalizedMessage()), ex);
            return new StepResult(StepExitCode.FAILED_NO_UNDO);
        }

        if(executor == null) {
            protocol.log(Level.INFO, I18NManager.formatMessage("waitForExe.notFound", BUNDLE, hostname));
            return new StepResult(StepExitCode.REPEAT);
        }

        if(protocol.isLoggable(Level.FINE)) {
            protocol.log(Level.FINE, I18NManager.formatMessage("waitForExe.query", BUNDLE, hostname));
        }
        
        ComponentState state;
        try {
            state = executor.getState();
        } catch (GrmRemoteException ex) {
            protocol.log(Level.SEVERE, I18NManager.formatMessage("waitForExe.query.ex", BUNDLE, hostname, ex.getLocalizedMessage()), ex);
            return new StepResult(StepExitCode.FAILED_NO_UNDO);
        }

        if (!ComponentState.STARTED.equals(state)) {
            protocol.log(Level.INFO, I18NManager.formatMessage("waitForExe.invalidState", BUNDLE, hostname, state, ComponentState.STARTED));
            return new StepResult(StepExitCode.REPEAT);
        }
        if(protocol.isLoggable(Level.FINE)) {
            protocol.log(Level.FINE, I18NManager.formatMessage("waitForExe.success", BUNDLE, hostname));
        }
        return new StepResult(StepExitCode.SUCCESS);
    }

    /**
     * This step does not need any undo functionality
     * @param ctx        the action context
     * @param params     the input parameter
     * @param protocol   the protocol handler
     * @return an empty map
     */
    public Map<String, Object> undo(GefActionContext ctx, Map<String, Object> params, Logger protocol) {
        return Collections.<String, Object>emptyMap();
    }

}
