/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.cloud;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceStoreException;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.adapter.InvalidResourceStateException;
import com.sun.grid.grm.resource.adapter.ResourceAdapterStore;
import com.sun.grid.grm.resource.adapter.impl.AbstractResourceAdapterFileStore;
import com.sun.grid.grm.resource.impl.FileResourceStore;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.descriptor.ResourceReassignmentDescriptor;
import java.io.File;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Logger;

/**
 * <p>CloudResourceAdapterStore is mainly introduced to persist data of the current cloud state
 * The remaining ResourceAdapterStore functionality is similar to the one from DefaultResourceAdapterStore </p>
 */
public class CloudResourceAdapterStore implements ResourceAdapterStore<ResourceId, CloudResourceAdapter> {

    private static final String BUNDLE = "com.sun.grid.grm.service.impl.cloud.messages";
    private static final Logger log = Logger.getLogger(CloudResourceAdapterStore.class.getName(), BUNDLE);
    private final CloudServiceAdapterImpl cloudService;
    private final int maxCloudHostsInSystemLimit;
    private final InnerCloudResourceAdapterStore internalStore;
    private final InnerCloudResourceAdapterStore externalStore;
    /**
     * synchronization over both stores
     */
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    /**
     * Constructor
     * @param cloudService to pass it into the ResourceAdapters
     * @param spoolDir to persist data of the cloud state
     * @param maxCloudHostsInSystemLimit the max number of cloud hosts in the system
     */
    public CloudResourceAdapterStore(CloudServiceAdapterImpl cloudService, File spoolDir, int maxCloudHostsInSystemLimit) {
        log.entering(CloudResourceAdapterStore.class.getName(), "constructor", spoolDir);
        internalStore = new InnerCloudResourceAdapterStore(new File(spoolDir, "intern"));
        externalStore = new InnerCloudResourceAdapterStore(new File(spoolDir, "extern"));

        this.cloudService = cloudService;
        this.maxCloudHostsInSystemLimit = maxCloudHostsInSystemLimit;
        log.exiting(CloudResourceAdapterStore.class.getName(), "constructor");
    }

    /**
     * The cloud adapter does not allow the freeing of any resources with the
     * free resource option when the service goes down.
     * @return empty set of cloud resource adapters
     */
    public Set<CloudResourceAdapter> getResourceAdaptersToFree() {
        return Collections.<CloudResourceAdapter>emptySet();
    }

    /**
     * Create a resource adapter for a resource
     * @param resource the resource
     * @return the resource adapter
     */
    public CloudResourceAdapter createResourceAdapter(Resource resource) {
        return internalStore.createResourceAdapter(resource);
    }

    /**
     * Get a resource adapter from the internal storage
     * @param resourceId the resource id
     * @return the resource adapter from the internal storage
     * @throws com.sun.grid.grm.resource.ResourceStoreException  if the internal storage has a problem
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is not storted in the internal storage
     */
    public CloudResourceAdapter get(ResourceId resourceId) throws ResourceStoreException, UnknownResourceException {
        lock.readLock().lock();
        try {
            return internalStore.get(resourceId);
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get a resource adapter from the external storage
     * @param resourceId the resource id
     * @return the resource adapter from the external storage
     * @throws com.sun.grid.grm.resource.ResourceStoreException  if the external storage has a problem
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is not storted in the external storage
     */
    CloudResourceAdapter getExternal(ResourceId resourceId) throws ResourceStoreException, UnknownResourceException {
        lock.readLock().lock();
        try {
            return externalStore.get(resourceId);
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get the ResourceAdapter for a Resource from the internal storage
     * @param key  the key of the resource
     * @return  the ResourceAdapter
     * @throws com.sun.grid.grm.resource.ResourceStoreException  if the ResourceAdapter could not be loaded
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is not stored
     */
    public CloudResourceAdapter getByKey(ResourceId key) throws ResourceStoreException, UnknownResourceException {
        lock.readLock().lock();
        try {
            return internalStore.getByKey(key);
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get all stored ResourceAdapters from the internal storage.
     * @return Set of ResourceAdapters
     * @throws com.sun.grid.grm.resource.ResourceStoreException on any storage exception
     */
    public Set<CloudResourceAdapter> getResourceAdapters() throws ResourceStoreException {
        lock.readLock().lock();
        try {
            return internalStore.getResourceAdapters();
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get the ids of all stored Resources from the internal storage
     * @return set of resource ids
     * @throws com.sun.grid.grm.resource.ResourceStoreException on any storage problem
     */
    public Set<ResourceId> getResourceIds() throws ResourceStoreException {
        lock.readLock().lock();
        try {
            return internalStore.getResourceIds();
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get all stored Resources from the internal storage
     * @return set of Resources
     * @throws com.sun.grid.grm.resource.ResourceStoreException
     */
    public Set<Resource> getResources() throws ResourceStoreException {
        lock.readLock().lock();
        try {
            return internalStore.getResources();
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Load all ResourceAdapter from the persistent storage
     * @throws com.sun.grid.grm.resource.ResourceStoreException if the ResourceAdapter could not be loaded
     */
    public void load() throws ResourceStoreException {
        lock.writeLock().lock();
        try {
            internalStore.load();
            externalStore.load();
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * must be called with (at least) readLock held
     */
    private int getCloudHostsInSystemCount() {
        int ret = externalStore.size();
        for (CloudResourceAdapter ra : internalStore.getResourceAdapters()) {
            if (ra.isPreparedForUninstall()) {
                ret++;
            }
        }
        return ret;
    }

    /**
     * Mark a resource as prepared for bind (startup).
     * @param ra the resoure adapter or the resource
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException if the maxCloudHostsInSystemLimit is reached
     */
    void markResourceAsPreparedForBind(CloudResourceAdapter ra) throws InvalidResourceStateException {
        lock.writeLock().lock();
        try {
            if (getCloudHostsInSystemCount() >= maxCloudHostsInSystemLimit) {
                throw new InvalidResourceStateException("CloudResourceAdapterStore.maxReached", BUNDLE, maxCloudHostsInSystemLimit);
            }
            ra.setPreparedForUninstall(true);
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Remove a resource from the internal storage, store in external storage if
     * the resource is reassigned.
     * @param resourceId  the id of the resource
     * @param descr       the removal descriptor
     * @return the adapter of the removed resource
     * @throws com.sun.grid.grm.resource.ResourceStoreException if the Resource could not be removed
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the Resource is not stored
     */
    public CloudResourceAdapter remove(ResourceId resourceId, ResourceRemovalDescriptor descr) throws ResourceStoreException, UnknownResourceException {
        lock.writeLock().lock();
        try {
            CloudResourceAdapter ra = internalStore.remove(resourceId, descr);
            ra.setPreparedForUninstall(false);
            if (descr instanceof ResourceReassignmentDescriptor) {
                // special semantic, move from internal to external store
                externalStore.store(ra);
            }
            return ra;
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Get the number resources stored resources in the internal storage
     * @return the number of stored resources
     */
    public int size() {
        lock.readLock().lock();
        try {
            return internalStore.size();
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Store a resource.
     * 
     * @param ra the adapter of the resource
     * @throws com.sun.grid.grm.resource.ResourceStoreException if the resource could not be stored (e.g. disk full)
     */
    public void store(CloudResourceAdapter ra) throws ResourceStoreException {
        lock.writeLock().lock();
        try {
            // try to remove from external
            try {
                externalStore.remove(ra.getId(), ResourceReassignmentDescriptor.getInstance(false));
            } catch (UnknownResourceException ex) {
                // ignore
            }
            // We can reset now the prepared for uninstall flag. It guarantees only
            // for the short time from calling prepareUninstall to the commit of the
            // resource transaction that the resource is consider for the max cloud
            // in system limit.
            // Once the resource is in UNASSIGNING state it is considered anyway.
            ra.setPreparedForUninstall(false);
            internalStore.store(ra);
        } finally {
            lock.writeLock().unlock();
        }
    }

    private class InnerCloudResourceAdapterStore
            extends AbstractResourceAdapterFileStore<ResourceId, CloudResourceAdapter, Resource> {

        private InnerCloudResourceAdapterStore(File spoolDir) {
            super(new FileResourceStore(spoolDir));
        }

        /**
         * Creates a CloudResourceAdapter containing the resource
         * @param elem  the resource
         * @return the DefaultResourceAdpater
         */
        @Override
        protected CloudResourceAdapter createResourceAdapterForElement(Resource elem) {
            return new CloudResourceAdapter(cloudService, elem);
        }

        /**
         * Get the resource from the adapter
         * @param adapter the adapter
         * @return the resource
         */
        @Override
        protected Resource getElementFromResourceAdapter(CloudResourceAdapter adapter) {
            return adapter.getResource();
        }

        /**
         * Creates a DefaultResourceAdpater containing the resource
         * @param resource  the resource
         * @return the DefaultResourceAdpater
         */
        public CloudResourceAdapter createResourceAdapter(Resource resource) {
            return new CloudResourceAdapter(cloudService, resource);
        }
    }
}

