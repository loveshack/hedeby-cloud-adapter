/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.cloud.steps;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.gef.GefActionContext;
import com.sun.grid.grm.gef.StepExitCode;
import com.sun.grid.grm.gef.StepResult;
import com.sun.grid.grm.util.I18NManager;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  This step detects wheter the SDM system is SSL protected. It sets in the
 *  output parameter the value of the key sdm_no_sslo to true if the SDM system
 *  is not SSL protected. Otherwise it sets the key to true.
 *
 */
public final class SetGlobalParametersStep extends AbstractJavaStep {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.cloud.steps.service_impl_cloud_steps";

    private final static String NO_SSL = "sdm_no_ssl";

    /**
     * Create a new instance of SetGlobalParametersStep
     * @param name the name of the step
     */
    public SetGlobalParametersStep(String name) {
        super(name);
    }

    /**
     * Execute this step
     * @param ctx              the action context
     * @param inputParams      the input parameters
     * @param protocol         the protocol logger
     * @return the step result
     */
    public StepResult execute(GefActionContext ctx, Map<String, Object> inputParams, Logger protocol) {
        ExecutionEnv env  = ctx.getExecutionEnv();
        
        Boolean noSSL = (Boolean)env.getProperties().get(ExecutionEnv.NO_SSL);
        if (noSSL == null) {
            protocol.log(Level.WARNING, I18NManager.formatMessage("setGlobalParams.noSSL", BUNDLE));
            noSSL = Boolean.FALSE;
        }
        return new StepResult(StepExitCode.SUCCESS, Collections.<String,Object>singletonMap(NO_SSL, noSSL));
    }

    /**
     * Set the sdm_no_ssl parameter to null in the output parameters.
     *
     * @param ctx              the action context
     * @param inputParams      the input parameters
     * @param protocol         the protocol logger
     * @return the output parameters
     */
    public Map<String, Object> undo(GefActionContext ctx, Map<String, Object> inputParams, Logger protocol) {
        return Collections.<String,Object>singletonMap(NO_SSL, null);
    }

}
