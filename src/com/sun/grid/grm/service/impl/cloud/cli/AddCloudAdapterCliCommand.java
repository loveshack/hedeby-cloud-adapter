/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/ 

package com.sun.grid.grm.service.impl.cloud.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.config.XMLUtil;
import com.sun.grid.grm.config.common.PermanentRequestSLOConfig;
import com.sun.grid.grm.config.common.SLOSet;
import com.sun.grid.grm.config.cloud.CloudAdapterConfig;
import com.sun.grid.grm.config.cloud.CloudGefConfig;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.service.impl.cloud.CloudServiceImpl;
import com.sun.grid.grm.ui.component.AddComponentWithConfigurationCommand;
import com.sun.grid.grm.ui.component.InstantiateComponentCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.validate.ValidatorService;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *  This cli command is responsible for adding a cloud adapter service 
 *  to the system
 */
@CliCommandDescriptor(
    name = "AddCloudAdapterCliCommand",
    hasShortcut = true,
    category = CliCategory.SERVICES,
    bundle = "com.sun.grid.grm.service.impl.cloud.cli.messages",
    requiredPrivileges = { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR }
)
public class AddCloudAdapterCliCommand extends AbstractSortedTableCliCommand {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.cloud.cli.messages";
    private final static Logger log = Logger.getLogger(AddCloudAdapterCliCommand.class.getName(), BUNDLE);
    
    @CliOptionDescriptor(name = "AddCloudAdapterCliCommand.j",
    numberOfOperands = 1,
    required = true)
    private String jvmName;
    @CliOptionDescriptor(name = "AddCloudAdapterCliCommand.h",
    numberOfOperands = 1,
    required = true)
    private String host;
    @CliOptionDescriptor(name = "AddCloudAdapterCliCommand.n",
    numberOfOperands = 1,
    required = true)
    private String name;
    @CliOptionDescriptor(name = "AddCloudAdapterCliCommand.start",
    numberOfOperands = 0)
    private boolean start;

    @CliOptionDescriptor(name = "AddCloudAdapterCliCommand.type", numberOfOperands = 1,
    required = true)
    private String type;
    
    //Optional parameter for import configuration file
    @CliOptionDescriptor(
    name = "AddCloudAdapterCliCommand.f",
            numberOfOperands = 1
            )
    private String fileName = null;
    
    private static final String errorBundle = "com.sun.grid.grm.cli.client";
    private final String empty = I18NManager.formatMessage("AddCloudAdapterCliCommand.empty", getBundleName());
    
    public void execute(AbstractCli cli) throws GrmException {

        Throwable error = null;
        ExecutionEnv env = cli.getExecutionEnv();
        
        
        
        /* Convert any provided hostname string to the canonical name if possible.
           This especially covers the case where the provided hostname is 'localhost' */
        Hostname temp = Hostname.getInstance(host);
        if (temp.isResolved()) {
            host = temp.getHostname();
        } else {
            throw new GrmException("AddCloudAdapterCliCommand.failed.host", getBundleName(), name, host);
        }
        
        CloudAdapterConfig cloudConf=null;
        
          
         // try to import configuration file
        if (fileName != null) {
            File tmpFile = new File(fileName);
            if (!tmpFile.exists()) {
                throw new GrmException("AddCloudAdapterCliCommand.missing_importfile", getBundleName(), fileName);
            }
            try {
                cloudConf = (CloudAdapterConfig)XMLUtil.loadAndValidate(tmpFile);
                ValidatorService.validateChain(env, cloudConf);        
            }catch(ClassCastException e){
                throw new GrmException("AddCloudAdapterCliCommand.wrong_xml_config", getBundleName());
            }
        } else {
            cloudConf = getDefaultCloudAdapterConfig(cli.getExecutionEnv(), name, type);
            cloudConf = XMLUtil.<CloudAdapterConfig>edit(env, cloudConf, "cloud_config");
            if(cloudConf == null) {
                cli.out().println("AddCloudAdapterCliCommand.unchanged", getBundleName());//BUNDLE?!!
                return;
            }
        }
        //Add entry to global and component configuration
        AddComponentWithConfigurationCommand aeCmd =
                AddComponentWithConfigurationCommand.newInstanceForSingleton().componentName(name).classname(CloudServiceImpl.class.getName()).config(cloudConf).hostname(host).jvmName(jvmName);
        //add service to Global config and create service config

        try {
            cli.getExecutionEnv().getCommandService().execute(aeCmd);
        } catch (Throwable ex) {
            error = ex;
        }

        if (error == null && start) {
            try {
                InstantiateComponentCommand cmd = new InstantiateComponentCommand();
                cmd.setComponentName(name);                
                cmd.setHostName(host);
                cmd.setJvmName(jvmName);
                cli.getExecutionEnv().getCommandService().execute(cmd);
            } catch (Throwable ex) {
                error = new GrmException("AddCloudAdapterCliCommand.failed.start", ex, getBundleName());
            }
        }


        TableModel tm = new TableModel(name, host, jvmName, error, cli);
        Table table = createTable(tm, cli);
        table.print(cli);

        //check whether any error
        if (error != null) {
            throw new GrmException("client.error.cli_error", errorBundle);
        }
    }

    private class TableModel extends AbstractDefaultTableModel {
        private final static long serialVersionUID = -20091001L;
        private String name = null;
        private String host = null;
        private String jvmName = null;
        private Throwable error = null;

        public TableModel(String name, String host, String jvmName, Throwable error, AbstractCli cli) {
            super(false,true,false,cli.getDebug());
            this.name = name;
            this.host = host;
            this.jvmName = jvmName;
            this.error = error;
        }

        public int getRowCount() {
            return 1;
        }

        public int getColumnCount() {
            return 4;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return name;
                case 1:
                    return host;
                case 2:
                    return jvmName;
                case 3:
                    if (error == null) {
                        return empty;
                    }
                    return error.getLocalizedMessage();
                default:
                    throw new IllegalArgumentException("unknown column " + columnIndex);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return getBundle().getString("AddCloudAdapterCliCommand.col.name");
                case 1:
                    return getBundle().getString("AddCloudAdapterCliCommand.col.host");
                case 2:
                    return getBundle().getString("AddCloudAdapterCliCommand.col.jvm");
                case 3:
                    return getBundle().getString("AddCloudAdapterCliCommand.col.message");
                default:
                    throw new IllegalArgumentException("unknown column " + column);
            }
        }

        @Override
        public String getValueForDebugColumn(int row) {
            String ret = "";
            return errorStackTraceToString(error);
        }
    }

    public static CloudAdapterConfig getDefaultCloudAdapterConfig(ExecutionEnv env, String serviceName, String cloudType) throws GrmException {

          //Prepare the cloud adapter Configuration for component
        CloudAdapterConfig cloudConf = new CloudAdapterConfig();
        // prepare SLO config
        PermanentRequestSLOConfig sloConf = new PermanentRequestSLOConfig();
        //default is 2
        sloConf.setUrgency(2);

        // create a condigtion for filtering the affected resources
        sloConf.setName("PermanentRequestSLO");

        String filterStr = String.format("%s = \"%s\" & owner = \"%s\"",
                ResourceVariableResolver.TYPE_VAR, HostResourceType.HOST_TYPE, serviceName);
        sloConf.setRequest(filterStr);
        sloConf.setResourceFilter(filterStr);

        // assign slo to cloud adapterconfig
        SLOSet sloset = new SLOSet();
        sloset.getSlo().add(sloConf);
        cloudConf.setSlos(sloset);

        cloudConf.setMaxCloudHostsInSystemLimit(10);

        File cloudBaseDir = new File(env.getDistDir(), "util/cloud/".replace('/', File.separatorChar));
        File cloudTypeDir = new File(cloudBaseDir, cloudType);

        File gefConfFile = new File(cloudTypeDir, "gef.xml");
        log.log(Level.FINE, "AddCloudAdapterCliCommand.loadGEF", gefConfFile);
        CloudGefConfig gef;
        try {
            gef = (CloudGefConfig) XMLUtil.load(gefConfFile);
        } catch (ClassCastException ex) {
            throw new GrmException("AddCloudAdapterCliCommand.ex.loadGEF", ex, BUNDLE, cloudType, gefConfFile, ex.getLocalizedMessage());
        }
        cloudConf.setGef(gef);

        return cloudConf;

    }
}
