/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.cloud;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.cloud.CloudAdapterConfig;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.validate.GrmValidationException;
import com.sun.grid.grm.validate.Validation;
import com.sun.grid.grm.validate.Validator;
import java.util.LinkedList;
import java.util.List;

@Validation(type = CloudAdapterConfig.class)
public class CloudAdapterConfigValidator implements Validator<CloudAdapterConfig> {

    private static final String BUNDLE = "com.sun.grid.grm.service.impl.cloud.messages";
    
    /**
     * Validates the configuration for a cloud adapter
     * @param env 
     * @param newConf
     * @param old 
     * @throws com.sun.grid.grm.validate.GrmValidationException
     */
    public void validate(ExecutionEnv env, CloudAdapterConfig newConf, CloudAdapterConfig old) throws GrmValidationException {
        // Validate that the mapping for the config exists
        List<String> errors = new LinkedList<String>();

        // TODO define validator for the different cloud types

        if (errors.size() > 0) {
            throw new GrmValidationException(errors);
        }
    }
    private void testForDefaultStrings(List<String> errors, String name, String valueStr, String defaultStr) {
        if (valueStr == null) {
            String msg = format("Please_enter_a_propper_value_for_configuration_parameter_{0}.", name);
            errors.add(msg);
        } else if (defaultStr.equals(valueStr)) {
            String msg = format("Please_replace_value_place_holder_{0}_of_configuration_parameter_{1}_with_a_propper_value.", defaultStr, name);
            errors.add(msg);
        }
    }

    private String format(String messageKey, Object... params) {
        return I18NManager.formatMessage(messageKey, BUNDLE, params);
    }
}
