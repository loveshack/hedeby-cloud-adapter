/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.cloud.executor;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.util.I18NManager;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @param <K> The type of the key which identifies queue for parallel tasks
 * 
 */
public class TaskQueueExecutor<K> {

    private static final String BUNDLE = "com.sun.grid.grm.service.impl.cloud.executor.messages";
    private final static Logger log = Logger.getLogger(TaskQueueExecutor.class.getName());
    private final static String SEQUENTIAL_QUEUE = "SEQ";
    private ExecutorService executor;
    private final Lock lock = new ReentrantLock();
    private final Queue<Task> taskQueue = new LinkedList<Task>();
    private final AtomicLong taskSequence = new AtomicLong();
    private final ThreadFactory threadFactory;
    private Scheduler scheduler;

    /**
     * Create a new instance of TaskQueueExecutor
     * @param name the name of the TaskQueueExecutor. The name thread created by this executor
     *             contains this string
     */
    public TaskQueueExecutor(String name) {
        this(new DefaultThreadFactory(name));
    }

    /**
     * Create a new instance of TaskQueueExecutor
     * 
     * @param threadFactory the thread factory which used for creating threads
     */
    public TaskQueueExecutor(ThreadFactory threadFactory) {
        this.threadFactory = threadFactory;
    }

    /**
     *  Start the executor.
     * 
     * @throws AlreadyStartedException If the executor has already been started
     * @throws GrmException 
     */
    public void start() throws AlreadyStartedException, GrmException {
        log.entering(TaskQueueExecutor.class.getName(), "start");
        lock.lock();
        try {
            if (!isTerminated()) {
                throw new AlreadyStartedException();
            }
            executor = Executors.newCachedThreadPool(threadFactory);
            scheduler = new Scheduler();
            executor.submit(scheduler);
            //ensure that the scheduler is started => otherwise stopping executor 
            //service will cause problems
            try {
                scheduler.waitForStarted(60000);
            } catch (InterruptedException ex) {
                String msg = format("Error_starting_the_TaskQueueExecutor.");
                throw new GrmException(msg);
            }
        } finally {
            lock.unlock();
        }
        log.exiting(TaskQueueExecutor.class.getName(), "start");
    }

    /**
     * Stop the executor.
     * 
     * <p>
     *   If the parameter <code>forced</code> is false the executor will execute
     *   all pending task before terminating.
     * </p>
     * <p>
     *   If the parameter <code>forced</code> it true the executor will be stopped
     *   immediately. Pending jobs will not be executed.
     * </p>
     * @param forced forced mode
     */
    public void stop(boolean forced) {
        log.entering(TaskQueueExecutor.class.getName(), "stop", forced);
        lock.lock();
        try {
            if (!isTerminated()) {
                if (scheduler != null) {
                    scheduler.shutdown();
                }
                if (forced) {
                    executor.shutdownNow();
                }
                // Remove all pending tasks even if not forced
                for (Task task : taskQueue) {
                    task.clear();
                }
                taskQueue.clear();
            }
        } finally {
            lock.unlock();
        }
        log.exiting(TaskQueueExecutor.class.getName(), "stop");
    }

    /**
     * Stop the executor and wait until it is terminated
     * @param forced   forced mode
     * @param timeout  max waiting time
     * @param unit     the time unit of the timeout parameter
     * @return <code>true</code> if the executor has terminated with the specified timeout
     *         <code>false</code> if the timeout has elapsed
     * @throws java.lang.InterruptedException if the calling thread has been interrupted while waiting
     */
    public boolean stopAndWait(boolean forced, long timeout, TimeUnit unit) throws InterruptedException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(TaskQueueExecutor.class.getName(), "stopAndWait", new Object[]{forced, timeout, unit});
        }
        stop(forced);
        boolean ret = waitUntilTerminated(timeout, unit);
        log.exiting(TaskQueueExecutor.class.getName(), "stopAndWait", ret);
        return ret;
    }

    /**
     * Wait until the executor terminates
     * @param timeout  max waiting time
     * @param unit     the time unit of the timeout parameter
     * @return <code>true</code> if the executor has terminated with the specified timeout
     *         <code>false</code> if the timeout has elapsed
     * @throws java.lang.InterruptedException if the calling thread has been interrupted while waiting
     */
    public boolean waitUntilTerminated(long timeout, TimeUnit unit) throws InterruptedException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(TaskQueueExecutor.class.getName(), "waitUntilTerminated", new Object[]{timeout, unit});
        }
        ExecutorService exe = null;
        lock.lock();
        try {
            exe = executor;
        } finally {
            lock.unlock();
        }

        boolean ret = false;
        if (exe != null) {
            if (exe.isTerminated()) {
                ret = true;
            } else {
                String msg = format("Await_termination_of_executor");
                log.log(Level.FINE, msg);
                ret = exe.awaitTermination(timeout, unit);
            }
        } else {
            ret = true;
        }
        log.exiting(TaskQueueExecutor.class.getName(), "waitUntilTerminated", ret);
        return ret;
    }

    /**
     * Is the executor active.
     * 
     * @return <code>true</code> if the executor is active
     */
    public boolean isActive() {
        log.entering(TaskQueueExecutor.class.getName(), "isActive");
        boolean ret;

        lock.lock();
        try {
            ret = executor != null && !executor.isShutdown();
        } finally {
            lock.unlock();
        }
        log.exiting(TaskQueueExecutor.class.getName(), "isActive", ret);
        return ret;
    }

    /**
     * Is the executor terminated
     * @return <code>true</code> if the executor is terminated
     */
    public boolean isTerminated() {
        log.entering(TaskQueueExecutor.class.getName(), "isTerminated");
        boolean ret;

        lock.lock();
        try {
            ret = executor == null || executor.isTerminated();
        } finally {
            lock.unlock();
        }
        log.exiting(TaskQueueExecutor.class.getName(), "isTerminated", ret);
        return ret;

    }

    /**
     * Submit a parallel task into a queue.
     * 
     * @param key       the identifier of the queue
     * @param runnable  the task
     * @throws com.sun.grid.grm.service.impl.cloud.executor.NotActiveException if the executor has been stopped
     */
    @SuppressWarnings("unchecked")
    public void submitParallelTask(K key, Runnable runnable) throws NotActiveException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(TaskQueueExecutor.class.getName(), "submitParallelTask", new Object[]{key, runnable});
        }
        lock.lock();
        try {
            if (!isActive()) {
                throw new NotActiveException();
            }
            Task task = taskQueue.peek();
            if (task != null && task.isParallel()) {
                ((Task<K>) task).add(key, runnable);
            } else {
                task = this.<K>newParrallelInstance(executor, taskSequence.getAndIncrement());
                ((Task<K>) task).add(key, runnable);
                taskQueue.add(task);
                scheduler.trigger();
            }
        } finally {
            lock.unlock();
        }
        log.exiting(TaskQueueExecutor.class.getName(), "submitParallelTask");
    }

    /**
     * Submit a sequential task
     * @param runnable  the sequential task
     * @throws com.sun.grid.grm.service.impl.cloud.executor.NotActiveException if the executor has been stopped
     */
    public void submitSerialTask(Runnable runnable) throws NotActiveException {
        log.entering(TaskQueueExecutor.class.getName(), "submitSeriellTask", runnable);

        Task<String> task = this.<String>newSequentialInstance(executor, taskSequence.getAndIncrement());
        task.add(SEQUENTIAL_QUEUE, runnable);
        lock.lock();
        try {
            if (!isActive()) {
                throw new NotActiveException();
            }
            taskQueue.add(task);
            scheduler.trigger();
        } finally {
            lock.unlock();
        }
        log.exiting(TaskQueueExecutor.class.getName(), "submitSeriellTask");
    }

    /**
     * Submit a sequential task if it is not queued already/anymore
     * @param runnable  the sequential task
     * @throws com.sun.grid.grm.service.impl.cloud.executor.NotActiveException if the executor has been stopped
     */
    public void submitSerialTaskIfNotQueued(Runnable runnable) throws NotActiveException {
        log.entering(TaskQueueExecutor.class.getName(), "submitSerialTaskIfNotQueued", runnable);
        if (!isInQueue(runnable)) {
            submitSerialTask(runnable);
        }

        log.exiting(TaskQueueExecutor.class.getName(), "submitSerialTaskIfNotQueued");
    }

    public boolean isInQueue(Runnable runnable) {
        lock.lock();
        try {
            for (Task task : taskQueue) {
                if (task.isInTask(runnable)) {
                    return true;
                }
            }
        } finally {
            lock.unlock();
        }
        return false;
    }

    private String format(String messageKey, Object... params) {
        return I18NManager.formatMessage(messageKey, BUNDLE, params);
    }

    enum SchedulerState {

        STOPPED,
        STARTED,
        SHUTDOWN
    }
    // encapsulation of the state change to allow waiting for a state change!
    private class SchedulerStateControl {

        private volatile SchedulerState state = SchedulerState.STOPPED;
        private final Lock lock = new ReentrantLock();
        private final Condition condition = lock.newCondition();

        void setState(SchedulerState state) {
            lock.lock();
            try {
                this.state = state;
                condition.signalAll();
            } finally {
                lock.unlock();
            }

        }

        SchedulerState getState() {
            return state;
        }

        void waitForState(SchedulerState state, long timeout) throws InterruptedException {
            long endTime = System.currentTimeMillis() + timeout;
            while (!Thread.currentThread().isInterrupted() && this.state != state) {
                long rest = endTime - System.currentTimeMillis();
                if (rest <= 0) {
                    break;
                }
                lock.lock();
                try {
                    condition.await(rest, TimeUnit.MILLISECONDS);
                } finally {
                    lock.unlock();
                }

            }
        }
    }

    private class Scheduler implements Runnable {

        private final Condition trigger = lock.newCondition();
        SchedulerStateControl stateCtrl = new SchedulerStateControl();

        public void trigger() {
            lock.lock();
            try {
                trigger.signal();
            } finally {
                lock.unlock();
            }
        }

        public void shutdown() {
            if (stateCtrl.getState().equals(SchedulerState.STARTED)) {
                stateCtrl.setState(SchedulerState.SHUTDOWN);
                trigger();
            }
        }

        public void waitForStarted(long timeout) throws InterruptedException {
            stateCtrl.waitForState(SchedulerState.STARTED, timeout);
        }

        public void run() {
            log.entering(Scheduler.class.getName(), "run");
            stateCtrl.setState(SchedulerState.STARTED);

            try {
                while (!Thread.currentThread().isInterrupted()) {
                    Task task = null;
                    String msg = format("Waiting_for_task");
                    log.log(Level.FINE, msg);
                    lock.lock();
                    try {
                        if (executor == null || executor.isShutdown()) {
                            // Executors in shutdown mode, stop processing
                            break;
                        }
                        task = taskQueue.peek();
                        if (task == null) {
                            if (stateCtrl.getState().equals(SchedulerState.SHUTDOWN)) {
                                executor.shutdown();
                                break;
                            } else {
                                trigger.await();
                            }
                        }
                    } finally {
                        lock.unlock();
                    }
                    if (task != null) {
                        if (log.isLoggable(Level.FINE)) {
                            msg = format("Executing_task_{0}", task);
                            log.log(Level.FINE, msg);
                        }
                        try {
                            task.run();
                            if (log.isLoggable(Level.FINE)) {
                                msg = format("Task_{0}", task);
                                log.log(Level.FINE, msg);
                            }
                        } catch (NotActiveException ex) {
                            throw ex;
                        } catch (InterruptedException ex) {
                            throw ex;
                        } catch (Throwable ex) {
                            msg = format("Task_{0}_failed._Caused_by_{1}", task, ex.getMessage());
                            log.log(Level.SEVERE, msg, ex);
                        }
                    }
                    lock.lock();
                    try {
                        if (task != null) {
                            taskQueue.poll();
                        }
                    } finally {
                        lock.unlock();
                    }
                }
            } catch (NotActiveException ex) {
                String msg = format("Executor_has_been_stopped");
                log.warning(msg);
            } catch (InterruptedException ex) {
                String msg = format("Scheduler_has_been_interrupted");
                log.warning(msg);
            } finally {
                stateCtrl.setState(SchedulerState.STOPPED);
            }
            log.exiting(Scheduler.class.getName(), "run");
        }
    }

    /**
     * Create a new sequential task
     * @param <T>  the type of the task
     * @param executor
     * @param seqNr
     * @return
     */
    private <T> Task<T> newSequentialInstance(ExecutorService executor, long seqNr) {
        return new Task<T>(executor, false, seqNr);
    }

    private <T> Task<T> newParrallelInstance(ExecutorService executor, long seqNr) {
        return new Task<T>(executor, true, seqNr);
    }

    /**
     * This class provides the queue for the jobs
     * It is used for sequential and parallel jobs.
     * @param <T> identifier type for the queues
     */
    private class Task<T> {

        private final ExecutorService executor;
        private final Map<T, Worker> queueMap = new HashMap<T, Worker>();
        private final Condition workerFinished = lock.newCondition();
        private final boolean parallel;
        private final long seqNr;
        private boolean isActive;
        private final AtomicLong workerSequence = new AtomicLong();

        private Task(ExecutorService executor, boolean parallel, long seqNr) {
            this.executor = executor;
            this.parallel = parallel;
            this.seqNr = seqNr;
        }

        public boolean isParallel() {
            return parallel;
        }

        /**
         * Remove all pending runnables from the workers
         */
        public void clear() {
            lock.lock();
            try {
                for (Worker worker : queueMap.values()) {
                    worker.clear();
                }
            } finally {
                lock.unlock();
            }
        }

        /**
         * Queue a new Runnable
         * @param key  the identifier for the queue
         * @param r    the runnable
         * @throws com.sun.grid.grm.service.impl.cloud.executor.NotActiveException if the executor has been stopped
         */
        public void add(T key, Runnable r) throws NotActiveException {
            if (log.isLoggable(Level.FINER)) {
                log.entering(Task.class.getName(), "add", new Object[]{key, r});
            }

            lock.lock();
            try {
                Worker worker = queueMap.get(key);
                if (worker == null) {
                    worker = new Worker(key, workerSequence.getAndIncrement());
                    queueMap.put(key, worker);
                    if (log.isLoggable(Level.FINER)) {
                        String msg = format("{0}:_{1}_created", toString(), worker);
                        log.log(Level.FINER, msg);
                    }
                }
                worker.add(r);
                if (isActive && !worker.isActive()) {
                    worker.start();
                }

            } finally {
                lock.unlock();
            }
            log.exiting(Task.class.getName(), "add");
        }

        private boolean isInTask(Runnable r) {
            lock.lock();
            try {
                for (Worker worker : queueMap.values()) {
                    if (worker.isInWorker(r)) {
                        return true;
                    }
                }
            } finally {
                lock.unlock();
            }
            return false;
        }

        /**
         * Run all workers
         * 
         * @throws com.sun.grid.grm.service.impl.cloud.executor.NotActiveException if the executor has been stopped
         * @throws java.lang.InterruptedException if the executor is stopped in force mode and there was active
         *                                           workers
         */
        public void run() throws NotActiveException, InterruptedException {
            log.entering(Task.class.getName(), "run");
            try {
                lock.lock();
                try {
                    for (Worker worker : queueMap.values()) {
                        worker.start();
                    }
                    isActive = true;
                    while (!queueMap.isEmpty()) {
                        workerFinished.await();
                    }
                } finally {
                    isActive = false;
                    lock.unlock();
                }
            } catch (RejectedExecutionException ex) {
                throw new NotActiveException();
            }
            log.exiting(Task.class.getName(), "run");
        }

        @Override
        public String toString() {
            if (parallel) {
                return String.format("PTask[%d]", seqNr);
            } else {
                return String.format("STask[%d]", seqNr);
            }
        }

        /**
         * The Worker is the Runnable with executes all Runnables from one
         * queue.
         */
        private class Worker implements Runnable {

            private final Queue<Runnable> queue = new LinkedList<Runnable>();
            private final long workerSeqNr;
            private final T key;
            private volatile Future future;

            public Worker(T key, long workerSeqNr) {
                this.key = key;
                this.workerSeqNr = workerSeqNr;
            }

            public void clear() {
                lock.lock();
                try {
                    queue.clear();
                } finally {
                    lock.unlock();
                }
            }

            @Override
            public String toString() {
                return String.format("worker[%s,%d]", key, workerSeqNr);
            }

            public T getKey() {
                return key;
            }

            public void add(Runnable r) {
                log.entering(Worker.class.getName(), "add", r);
                lock.lock();
                try {
                    queue.add(r);
                } finally {
                    lock.unlock();
                }
                log.exiting(Worker.class.getName(), "add");
            }

            private boolean isInWorker(Runnable r) {
                lock.lock();
                try {
                    return queue.contains(r);
                } finally {
                    lock.unlock();
                }
            }

            public boolean isActive() {
                Future f = future;
                return f != null;
            }

            public void start() throws NotActiveException {
                if (future == null) {
                    if (log.isLoggable(Level.FINER)) {
                        String msg = format("{0}.{1}:_Starting", Task.this.toString(), toString());
                        log.log(Level.FINER, msg);
                    }
                    try {
                        future = executor.submit(this);
                    } catch (RejectedExecutionException ex) {
                        String msg = format("executor_has_been_stopped");
                        throw new NotActiveException(msg, ex);
                    }
                }
            }

            public void run() {
                log.entering(Worker.class.getName(), "run");

                if (log.isLoggable(Level.FINE)) {
                    String msg = format("{0}.{1}:_started", Task.this.toString(), toString());
                    log.log(Level.FINE, msg);
                }
                while (!Thread.currentThread().isInterrupted()) {
                    Runnable r = null;
                    lock.lock();
                    try {
                        r = queue.peek();
                    } finally {
                        lock.unlock();
                    }
                    if (r != null) {
                        if (log.isLoggable(Level.FINE)) {
                            String msg = format("{0}.{1}:_Running_task_{2}", Task.this.toString(), toString(), r);
                            log.log(Level.FINE, msg);
                        }
                        try {
                            r.run();
                            if (log.isLoggable(Level.FINE)) {
                                String msg = format("{0}.{1}:_finished_{2}", Task.this.toString(), toString(), r);
                                log.log(Level.FINE, msg);
                            }
                        } catch (Throwable ex) {
                            String msg = format("{0}.{1}:_failed_{2}_Caused_by{3}", Task.this.toString(), toString(), r,ex.getLocalizedMessage());
                            log.log(Level.SEVERE, msg, ex);
                        }
                        lock.lock();
                        try {
                            queue.poll();
                        } finally {
                            lock.unlock();
                        }
                    } else {
                        lock.lock();
                        try {
                            if (queue.isEmpty()) {
                                future = null;
                                queueMap.remove(key);
                                workerFinished.signalAll();
                                break;
                            }
                        } finally {
                            lock.unlock();
                        }
                    }
                }
                if (log.isLoggable(Level.FINER)) {
                    String msg = format("{0}.{1}:_finished", Task.this.toString(), toString());
                    log.log(Level.FINE, msg);
                }
                log.exiting(Worker.class.getName(), "run");
            }
        }
    }
}
