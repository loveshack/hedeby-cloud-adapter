/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.cloud;

import com.sun.grid.grm.gef.GefAction;
import com.sun.grid.grm.gef.service.AbstractGefResourceAdapter;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceStoreException;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.adapter.InvalidResourceStateException;
import com.sun.grid.grm.resource.adapter.RAOperationResult;
import com.sun.grid.grm.resource.adapter.impl.AddResourceOPR;
import com.sun.grid.grm.resource.adapter.impl.NullOPR;
import com.sun.grid.grm.resource.adapter.impl.RemoveResourceOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceAddedOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceErrorOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceRemovedOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceRemovedOPRFactory;
import com.sun.grid.grm.resource.adapter.impl.ResoureAddedOPRFactory;
import com.sun.grid.grm.resource.adapter.impl.ResoureResetOPRFactory;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.descriptor.ResourcePurgeDescriptor;
import com.sun.grid.grm.service.descriptor.ResourceReassignmentDescriptor;
import com.sun.grid.grm.service.descriptor.ResourceRemovalFromSystemDescriptor;
import com.sun.grid.grm.util.I18NManager;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>
 * A resource Adapter that allows to shut down cloud hosts before the resource is
 * removed from the SDM system.
 * </p>
 */
public class CloudResourceAdapter extends AbstractGefResourceAdapter<ResourceId> {

    private static final String BUNDLE = "com.sun.grid.grm.service.impl.cloud.messages";
    private static final Logger log = Logger.getLogger(CloudResourceAdapter.class.getName(), BUNDLE);
    final static String RESOURCE_PROPERTY_OWNER = "owner";
    private final CloudServiceAdapterImpl cloudService;
    private volatile boolean preparedForUninstall;

    /**
     * Constructor
     * @param cloudService
     * @param resource
     */
    public CloudResourceAdapter(CloudServiceAdapterImpl cloudService, Resource resource) {
        super(cloudService, resource);
        this.cloudService = cloudService;
    }

    /**
     * Get the key of a of the resource
     * @return the hostname of the resource
     */
    public ResourceId getKey() {
        return resource.getId();
    }

    /**
     * prepare a resource for installation.
     *
     * For the cloud adapter installation means shut the resource down
     *
     * @return an AddResourceOPR
     *
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException
     */
    @Override
    protected RAOperationResult doPrepareInstall() throws InvalidResourceStateException {
        log.entering(CloudResourceAdapter.class.getName(), "doPrepareInstall");
        RAOperationResult ret;
        resourceStateLock.lock();
        try {
            Object owner = resource.getProperties().get(RESOURCE_PROPERTY_OWNER);
            if (!cloudService.getName().equals(owner)) {
                throw new InvalidResourceStateException("CloudResourceAdapter.ex.notOwner", BUNDLE, owner);
            }

            if (resource.isBound()) {
                try {
                    cloudService.getCloudResourceAdapterStore().getExternal(resource.getId());
                } catch (ResourceStoreException ex) {
                    throw new InvalidResourceStateException("CloudResourceAdapter.ex.storeError", ex, BUNDLE, ex.getLocalizedMessage());
                } catch (UnknownResourceException ex) {
                    throw new InvalidResourceStateException("CloudResourceAdapter.ex.resNotInStore", ex, BUNDLE);
                } catch (ServiceNotActiveException ex) {
                    throw new InvalidResourceStateException("CloudResourceAdapter.ex.asna", ex, BUNDLE);
                }
            }

            // TODO check further resource properties (like AMI ID, instance id, ...)

            resource.setState(Resource.State.ASSIGNING);
            resource.setAnnotation("");
            // We set the usage to the maximum, the next
            // SLO calculation will override it
            resource.setUsage(Usage.MAX_VALUE);

            ret = new AddResourceOPR(resource.clone(), resource.getAnnotation());
        } finally {
            resourceStateLock.unlock();
        }
        log.exiting(CloudResourceAdapter.class.getName(), "doPrepareInstall", ret);
        return ret;
    }

    /**
     * Prepare the reset of the resource
     * @return  an AddResourceOPR if the resetAction is configured otherwise a NullOPR
     */
    @Override
    protected RAOperationResult doPrepareReset() {
        log.entering(CloudResourceAdapter.class.getName(), "doPrepareReset");
        RAOperationResult ret = NullOPR.getInstance();
        try {
            GefAction resetAction = cloudService.getGef().getResetAction();
            if (resetAction != null) {
                resourceStateLock.lock();
                try {
                    resource.setState(Resource.State.ASSIGNING);
                    resource.setAnnotation("");
                    ret = new AddResourceOPR(resource.clone(), resource.getAnnotation());
                } finally {
                    resourceStateLock.unlock();
                }
            }
        } catch (ServiceNotActiveException ex) {
            log.log(Level.WARNING, I18NManager.formatMessage("CloudResourceAdapter.rsna", BUNDLE, cloudService.getName(), resource.toString()), ex);
        }
        log.exiting(CloudResourceAdapter.class.getName(), "doPrepareReset", ret);
        return ret;
    }

    /**
     * set the preparedForUninstall
     *
     * <p>All resources with the a set preparedForUninstall are considered for the
     *    max cloud hosts is system limit.</p>
     * 
     * @param preparedForUninstall the new value for the prepared for uninstall flag
     */
    void setPreparedForUninstall(boolean preparedForUninstall) {
        log.entering(CloudResourceAdapter.class.getName(), "setPreparedForUninstall", preparedForUninstall);
        this.preparedForUninstall = preparedForUninstall;
        log.exiting(CloudResourceAdapter.class.getName(), "setPreparedForUninstall");
    }

    /**
     * get the value of the preparedForUninstall flag
     * @return the value of the preparedForUninstall flag
     */
    boolean isPreparedForUninstall() {
        return preparedForUninstall || resource.getState().equals(Resource.State.UNASSIGNING);
    }

    /**
     * prepare the resource for uninstall.
     *
     * For the cloud adapter uninstall means that the virtual resource is started up
     * by executing the GEF startup action.
     *
     * @param descr the resource removal descriptor
     * @return a RemoveResourceOPR
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException if the resource can not be uninstalled
     *             (e.g max cloud resources is system is reached)
     */
    @Override
    protected RAOperationResult doPrepareUninstall(ResourceRemovalDescriptor descr) throws InvalidResourceStateException {
        log.entering(CloudResourceAdapter.class.getName(), "doPrepareUninstall", descr);

        String annotation = "";
        if (descr instanceof ResourceReassignmentDescriptor) {
            // markResourceAsPreparedForBind throws an exception
            // if maxCloudHostsInSystemLimit would be exceeded
            try {
                cloudService.getCloudResourceAdapterStore().markResourceAsPreparedForBind(this);
            } catch (ServiceNotActiveException ex) {
                throw new InvalidResourceStateException("CloudResourceAdapter.ex.usna", ex, BUNDLE);
            }
        } else if (descr instanceof ResourcePurgeDescriptor) {
            // purging of resource
            annotation = I18NManager.formatMessage("CloudResourceAdapter.purgeResource.anno", BUNDLE);
        }
        RAOperationResult ret;
        resourceStateLock.lock();
        try {
            resource.setState(Resource.State.UNASSIGNING);
            resource.setAnnotation(annotation);
            ret =  new RemoveResourceOPR(resource.clone(), resource.getAnnotation(), descr);
        } finally {
            resourceStateLock.unlock();
        }
        log.exiting(CloudResourceAdapter.class.getName(), "doPrepareUninstall", ret);
        return ret;
    }

    /**
     * Do the uninstall.
     *
     * The cloud adapter starts up a virtual resource if and only if the resource is moved
     * to a different service (not removed from the system).
     * @param descr removal descriptor (remove from system or move to different service)
     * @return the result of the uninstall
     */
    @Override
    protected RAOperationResult doUninstall(final ResourceRemovalDescriptor descr) {
        log.entering(CloudResourceAdapter.class.getName(), "doUninstall", descr);

        RAOperationResult ret = null;
        if (descr instanceof ResourceReassignmentDescriptor) {
            try {
                ResourceRemovedOPRFactory oprFac =  new ResourceRemovedOPRFactory(descr, I18NManager.formatMessage("CloudResourceAdapter.res.started", BUNDLE));
                GefAction action = cloudService.getGef().getStartupAction();
                ret = submitGefUpdateAction(action, oprFac);
            } catch (ServiceNotActiveException ex) {
                log.log(Level.WARNING, I18NManager.formatMessage("CloudResourceAdapter.usna", BUNDLE, cloudService.getName(), resource.toString()), ex);
                ret = NullOPR.getInstance();
            }
        } else if (descr instanceof ResourceRemovalFromSystemDescriptor || descr instanceof ResourcePurgeDescriptor) {
            resourceStateLock.lock();
            try {
                // for resource removal or purge from system, do nothing special
                resource.setState(Resource.State.UNASSIGNED);
                resource.setAnnotation("");
                ret = new ResourceRemovedOPR(resource.clone(), resource.getAnnotation(), descr);
            } finally {
                resourceStateLock.unlock();
            }
        } else {
            resourceStateLock.lock();
            try {
                resource.setState(Resource.State.ERROR);
                resource.setAnnotation(I18NManager.formatMessage("CloudResourceAdapter.uint.anno", BUNDLE));
                log.log(Level.SEVERE, I18NManager.formatMessage("CloudResourceAdapter.unit", BUNDLE, cloudService.getName(), resource.toString(), descr.getClass().getName()));
                ret = new ResourceErrorOPR(resource.clone(), resource.getAnnotation());
            } finally {
                resourceStateLock.unlock();
            }
        }

        log.exiting(CloudResourceAdapter.class.getName(), "doUninstall", ret);
        return ret;
    }

    
    /**
     * Install the resource.
     *
     * For the cloud adapter this calls the shutdown virtual host action
     * if and only if the resource was bound.
     * 
     * <p>Set the resources into ASSIGNED state.</p>
     * @return an ResourceAddedOPR
     */
    protected RAOperationResult doInstall() {
        log.entering(CloudResourceAdapter.class.getName(), "doInstall");
        RAOperationResult ret;
        if (resource.isBound()) {
            try {
                ResoureAddedOPRFactory oprFactory = new ResoureAddedOPRFactory(I18NManager.formatMessage("CloudResourceAdapter.res.stopped", BUNDLE));
                GefAction action = cloudService.getGef().getShutdownAction();
                ret = submitGefUpdateAction(action, oprFactory);
            } catch (ServiceNotActiveException ex) {
                log.log(Level.WARNING, I18NManager.formatMessage("CloudResourceAdapter.isna", BUNDLE, cloudService.getName(), resource.toString()), ex);
                ret = NullOPR.getInstance();
            }
        } else {
            resourceStateLock.lock();
            try {
                resource.setState(Resource.State.ASSIGNED);
                resource.setAnnotation(I18NManager.formatMessage("CloudResourceAdapter.newadd.anno", BUNDLE));
                ret = new ResourceAddedOPR(resource.clone(), resource.getAnnotation());
            } finally {
                resourceStateLock.unlock();
            }
        }
        log.exiting(CloudResourceAdapter.class.getName(), "doInstall", ret);
        return ret;
    }

    /**
     * Reset the resource.
     * 
     * @return a ResourceResetOPR
     */
    protected RAOperationResult doReset() {
        log.entering(CloudResourceAdapter.class.getName(), "doReset");
        RAOperationResult ret;
        try {
            GefAction resetAction = cloudService.getGef().getResetAction();
            if (resetAction != null) {
                ResoureResetOPRFactory oprFactory = new ResoureResetOPRFactory(I18NManager.formatMessage("CloudResourceAdapter.res.reset", BUNDLE));
                ret = submitGefUpdateAction(resetAction, oprFactory);
            } else {
                log.log(Level.WARNING, I18NManager.formatMessage("CloudResourceAdapter.noReset", BUNDLE, cloudService.getName(), resource.toString()));
                ret = NullOPR.getInstance();
            }
        } catch (ServiceNotActiveException ex) {
            log.log(Level.WARNING, I18NManager.formatMessage("CloudResourceAdapter.rsna", BUNDLE, cloudService.getName(), resource.toString()), ex);
            ret = NullOPR.getInstance();
        }
        log.exiting(CloudResourceAdapter.class.getName(), "doReset", ret);
        return ret;
    }



}
