/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2009 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/ 
package com.sun.grid.grm.service.impl.cloud.executor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 * Tests the TaskQueueExecutor
 */
public class TaskQueueExecutorTest extends TestCase {

    private final static Logger log = Logger.getLogger(TaskQueueExecutorTest.class.getName());
    private TaskQueueExecutor<String> exe;
    private ThreadGroup threadGroup;
    private History history;
    
    public TaskQueueExecutorTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        threadGroup = new ThreadGroup(getName());
        history = new History();
        exe = new TaskQueueExecutor<String>(new DefaultThreadFactory(getName(), threadGroup));
        exe.start();
        assertTrue("Executor must be active ", exe.isActive());
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        exe.stopAndWait(false, 20, TimeUnit.SECONDS);
        assertTrue("Executor did not terminate", exe.isTerminated());
        
        // Give the threadgroup some time to terminate
        for(int i = 0; i < 10; i++) {
            if (threadGroup.activeCount() == 0) {
                break;
            }
            Thread.sleep(100);
        }
        assertEquals("Not all threads of threadGroup " + threadGroup + " died", 0, threadGroup.activeCount());
    }

    public void testStartStart() throws Exception {
        log.entering(TaskQueueExecutorTest.class.getName(), "testStartStart");
        try {
            exe.start();
            fail("Start call on started executor must throw an AlreadyStartedException");
        } catch (AlreadyStartedException ex) {
            // fine
        }
        log.exiting(TaskQueueExecutorTest.class.getName(), "testStartStart");
    }
    
    public void testStoppedSubmitSequential() throws Exception {
        log.entering(TaskQueueExecutorTest.class.getName(), "testStoppedSubmitSequential");
        exe.stopAndWait(false, 10, TimeUnit.SECONDS);
        assertTrue("Executor did not terminate", exe.isTerminated());
        try {
            exe.submitSerialTask(new Job("s1", history));
            fail("submitSeriellTask on stopped executor must fail with an NotActiveException");
        } catch(NotActiveException ex) {
            // fine
        }
        log.exiting(TaskQueueExecutorTest.class.getName(), "testStoppedSubmitSequential");
    }
    
    public void testStoppedSubmitParrallel() throws Exception {
        log.entering(TaskQueueExecutorTest.class.getName(), "testStoppedSubmitParrallel");
        exe.stopAndWait(false, 10, TimeUnit.SECONDS);
        assertTrue("Executor did not terminate", exe.isTerminated());
        try {
            exe.submitParallelTask("q0", new Job("p0", history));
            fail("submitSeriellTask on stopped executor must fail with an NotActiveException");
        } catch(NotActiveException ex) {
            // fine
        }
        log.exiting(TaskQueueExecutorTest.class.getName(), "testStoppedSubmitParrallel");
    }
    
    
    public void testRestart() throws Exception {
        log.entering(TaskQueueExecutorTest.class.getName(), "testRestart");

        Job job = new Job("s0", history);
        exe.submitSerialTask(job);
        job.waitUntilRunning();
        exe.stopAndWait(false, 10, TimeUnit.SECONDS);
        assertEquals("job should be finished", Job.State.FINISHED, job.getState());
        
        exe.start();
        job = new Job("s1", history);
        exe.submitSerialTask(job);
        
        ExpectedHistory expHist = expect("s0", "s1");

        history.assertHistory("Got unexpected sequence", 5000, expHist);
        
        log.exiting(TaskQueueExecutorTest.class.getName(), "testRestart");
    }
    
    public void testStop() throws Exception {
        log.entering(TaskQueueExecutorTest.class.getName(), "testStop");
        
        Job s0 = new Job("s0", history).block();
        exe.submitSerialTask(s0);
        s0.waitUntilRunning();
        
        Job s1 = new Job("s1", history);
        exe.submitSerialTask(s1);
        exe.stop(false);
        
        s0.wakeup();
        
        exe.waitUntilTerminated(10, TimeUnit.SECONDS);
        
        assertEquals("Job s0 must be finshed", Job.State.FINISHED, s0.getState());
        assertEquals("Job s1 must not be finshed", Job.State.PENDING, s1.getState());
        log.exiting(TaskQueueExecutorTest.class.getName(), "testStop");
    }

    public void testStopForced() throws Exception {
        log.entering(TaskQueueExecutorTest.class.getName(), "testStop");
        
        Job s0 = new Job("s0", history).block();
        exe.submitSerialTask(s0);
        s0.waitUntilRunning();
        
        Job s1 = new Job("s1", history);
        exe.submitSerialTask(s1);
        exe.stop(true);
        
        s0.wakeup();
        
        exe.waitUntilTerminated(10, TimeUnit.SECONDS);
        
        assertEquals("Job s0 must be finished", Job.State.FINISHED, s0.getState());
        assertEquals("Job s1 must be finished", Job.State.PENDING, s1.getState());
        log.exiting(TaskQueueExecutorTest.class.getName(), "testStop");
    }
    
    public void testParallelInSingleQueue() throws Exception {
        log.entering(TaskQueueExecutorTest.class.getName(), "testParallelInSingleQueue");

        exe.submitParallelTask("q0", new Job("p0", history));
        exe.submitParallelTask("q0", new Job("p1", history));
        exe.submitParallelTask("q0", new Job("p2", history));
        exe.submitParallelTask("q0", new Job("p3", history));

        ExpectedHistory expHist =
                expect("p0", "p1", "p2", "p3");

        history.assertHistory("Got unexpected sequence", 5000, expHist);
        log.exiting(TaskQueueExecutorTest.class.getName(), "testParallelInSingleQueue");
    }

    public void testParallelWithRecreateQueue() throws Exception {
        log.entering(TaskQueueExecutorTest.class.getName(), "testParallelWithRecreateQueue");

        Job p0 = new Job("p0", history);
        exe.submitParallelTask("q0", p0);
        p0.waitUntilFinished();
        exe.submitParallelTask("q0", new Job("p1", history));

        ExpectedHistory expHist = expect("p0", "p1");

        history.assertHistory("Got unexpected sequence", 5000, expHist);
        log.exiting(TaskQueueExecutorTest.class.getName(), "testParallelWithRecreateQueue");
    }

    public void testSequentialOnly() throws Exception {
        log.entering(TaskQueueExecutorTest.class.getName(), "testSequentialOnly");
        assertTrue(exe.isActive());
        exe.submitSerialTask(new Job("s1", history));
        exe.submitSerialTask(new Job("s2", history));
        exe.submitSerialTask(new Job("s3", history));

        ExpectedHistory expHist = expect("s1", "s2", "s3");

        history.assertHistory("Got unexpected sequence", 5000, expHist);
        log.exiting(TaskQueueExecutorTest.class.getName(), "testSequentialOnly");
    }

    public void testSequence() throws Exception {
        log.entering(TaskQueueExecutorTest.class.getName(), "testSequence");

        exe.submitSerialTask(new Job("s0", history));
        exe.submitParallelTask("k0", new Job("p0", history));
        exe.submitParallelTask("k0", new Job("p1", history));
        exe.submitParallelTask("k1", new Job("p2", history));
        exe.submitParallelTask("k1", new Job("p3", history));
        exe.submitSerialTask(new Job("s1", history));

        ExpectedHistory expHist =
                expect(expect("s0"), expectAny("p0", "p1", "p2", "p3"), expect("s1"));

        history.assertHistory("Sleeper must run", 5000, expHist);
        log.exiting(TaskQueueExecutorTest.class.getName(), "testSequence");
    }

    private static class Job implements Runnable {

        enum State {
            PENDING,
            RUNNING,
            BLOCKED,
            FINISHED
        }
        private State state;
        private final String name;
        private final History history;
        private boolean blocked;
        
        public Job(String name, History history) {
            this.name = name;
            this.history = history;
            this.state = State.PENDING;
        }
        
        public synchronized Job block() {
            blocked = true;
            return this;
        }
        
        public synchronized void wakeup() {
            blocked = false;
            notifyAll();
        }

        private synchronized void setState(State state) {
            this.state = state;
            notifyAll();
        }
        
        public synchronized State getState() {
            return state;
        }

        public synchronized boolean waitUntilRunning() throws InterruptedException {
            while (true) {
                switch (state) {
                    case RUNNING:
                        return true;
                    case FINISHED:
                        return false;
                    default:
                        wait();
                }
            }
        }

        public synchronized boolean waitUntilFinished() throws InterruptedException {
            while (true) {
                switch (state) {
                    case FINISHED:
                        return true;
                    default:
                        wait();
                }
            }
        }

        public void run() {
            log.entering(Job.class.getName(), "run");
            setState(State.RUNNING);
            try {
                synchronized (this) {
                    while (blocked) {
                        wait();
                    }
                }
                Thread.sleep((int) (100 * Math.random()));
            } catch (InterruptedException ex) {
                //fine
            }
            history.add(name);
            setState(State.FINISHED);
            log.exiting(Job.class.getName(), "run");
        }

        @Override
        public String toString() {
            return "[Job: name=" + name + "]";
        }
    }

    private static class History {

        private final List<String> history = new LinkedList<String>();

        public synchronized void add(String s) {
            history.add(s);
            this.notifyAll();
        }

        public synchronized void assertHistory(String message, long timeout, ExpectedHistory expectedHistory) throws InterruptedException {

            long endTime = System.currentTimeMillis() + timeout;


            while (true) {
                boolean matches = expectedHistory.matches(new LinkedList<String>(history));
                log.log(Level.FINE, "" + expectedHistory + " machtes " + history.toString() + "? " + matches);
                if (matches) {
                    return;
                }
                long rest = endTime - System.currentTimeMillis();
                if (rest > 0) {
                    wait(rest);
                } else {
                    fail("Timeout while waiting for history " + expectedHistory + "(got " + history.toString() + ")");
                }
            }
        }
    }

    private static abstract class ExpectedHistory {

        public abstract boolean matches(List<String> history);

        public abstract int getSize();
    }

    private static ExpectedHistory expect(String s) {
        return new SimpleHistory(s);
    }

    private static class SimpleHistory extends ExpectedHistory {

        private final String expectedValue;

        public SimpleHistory(String expectedValue) {
            this.expectedValue = expectedValue;
        }

        @Override
        public boolean matches(List<String> history) {
            if (history.isEmpty()) {
                return false;
            }
            String value = history.remove(0);
            return expectedValue.equals(value);
        }

        public int getSize() {
            return 1;
        }

        @Override
        public String toString() {
            return expectedValue;
        }
    }

    private static ExpectedHistory expect(String... strs) {

        List<ExpectedHistory> ret = new ArrayList<ExpectedHistory>(strs.length);
        for (String s : strs) {
            ret.add(expect(s));
        }
        return new ExpectedSequence(ret);
    }

    private static ExpectedHistory expect(ExpectedHistory... exps) {
        List<ExpectedHistory> ret = new ArrayList<ExpectedHistory>(exps.length);
        for (ExpectedHistory s : exps) {
            ret.add(s);
        }
        return new ExpectedSequence(ret);
    }

    private static ExpectedHistory expectAny(String... strs) {
        return new ExpectAny(Arrays.asList(strs));
    }

    private static class ExpectedSequence extends ExpectedHistory {

        private final List<ExpectedHistory> elems;
        private final int size;

        public ExpectedSequence(List<ExpectedHistory> elems) {
            this.elems = elems;
            int s = 0;
            for (ExpectedHistory elem : elems) {
                s += elem.getSize();
            }
            this.size = s;
        }

        @Override
        public boolean matches(List<String> history) {
            for (ExpectedHistory elem : elems) {
                if (!elem.matches(history)) {
                    return false;
                }
            }
            return true;
        }

        @Override
        public int getSize() {
            return size;
        }

        @Override
        public String toString() {
            StringBuilder ret = new StringBuilder();
            ret.append("[");
            boolean first = true;
            for (ExpectedHistory elem : elems) {
                if (first) {
                    first = false;
                } else {
                    ret.append(",");
                }
                ret.append(elem.toString());
            }
            ret.append("]");
            return ret.toString();
        }
    }

    private static class ExpectAny extends ExpectedHistory {

        private final List<String> elems;

        public ExpectAny(List<String> elems) {
            this.elems = elems;
        }

        @Override
        public boolean matches(List<String> history) {
            if (history.size() < elems.size()) {
                return false;
            }
            List<String> sub = new ArrayList<String>(elems.size());
            for (int i = 0; i < elems.size(); i++) {
                sub.add(history.remove(0));
            }
            for (String elem : elems) {
                boolean found = false;
                for (int i = 0; i < sub.size(); i++) {
                    if (sub.get(i).equals(elem)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    return false;
                }
            }
            return true;
        }

        @Override
        public int getSize() {
            return elems.size();
        }

        @Override
        public String toString() {
            StringBuilder ret = new StringBuilder();
            ret.append("[");
            boolean first = true;
            for (String elem : elems) {
                if (first) {
                    first = false;
                } else {
                    ret.append("|");
                }
                ret.append(elem.toString());
            }
            ret.append("]");
            return ret.toString();
        }
    }
}
